<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class ChangePass extends CI_Controller {
 
 function __construct()
 {
   parent::__construct();
 }
 
 function index()
 {
  require_once('nusoap/nusoap.php');

    if($this->session->userdata('logged_in')) { 

      $password = $this->input->post('password');
      $user_pwd = md5($password);

      $this->load->library('form_validation');
      $this->form_validation->set_rules('password', 'Password', 'required',
          array('required' => 'You must provide a %s.')
      );
      $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');

      if($this->form_validation->run() == FALSE)
      {
        $this->load->view('changepwd_view');
      }
      else
      {  

            $check_data = $this->session->userdata('logged_in');

            $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');

            $error = $client->getError();
            if ($error) {
                die("client construction error: {$error}\n");
            }

            $answer = $client->call('ChangePwd',array('user_pwd' => $user_pwd, 'user_id' => $check_data['user_id']), '', '', false, true);

            $error = $client->getError();
            if ($error) {
                print_r($client->response);
                print_r($client->getDebug());
                die();
           } 


            redirect('Safe', 'refresh');
     }

    } else {
          redirect('Login', 'refresh');
    }
 
 }

function ResetPassword()
 {

          $this->load->view('resetpwd');
 
 }


}
?>