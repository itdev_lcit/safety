<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('Welcome.php');

class Detail extends Welcome {

	public function index(){

		if($this->session->userdata('logged_in')) { 	

			$check_data = $this->session->userdata('logged_in');
			
			$data['func'] = str_replace(' ', '', $check_data['func']);

			$this->view['main'] =  $this->load->view('subj/index',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}
}
