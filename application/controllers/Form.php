<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('Welcome.php');

class Form extends Welcome {

	public function index(){

		if($this->session->userdata('logged_in')) { 	

			$this->view['main'] =  $this->load->view('form/index','',true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}
}
