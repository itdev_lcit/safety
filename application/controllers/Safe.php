<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('Welcome.php');

class Safe extends Welcome {

	public function index(){

		if($this->session->userdata('logged_in')) { 	

			$this->InsertUsers();
			$check_data = $this->session->userdata('logged_in');
			$data['func'] = str_replace(' ', '', $check_data['func']);

			$this->view['main'] =  $this->load->view('safe/index',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		

	}

	public function Checkin(){

		if($this->session->userdata('logged_in')) { 	

			$this->InsertUsers();
			redirect('Safe', 'refresh');

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		

	}

	public function CheckList($id_loc = null){

		if($this->session->userdata('logged_in')) { 	

			$data['id_loc'] = $id_loc;
			
			$check_data = $this->session->userdata('logged_in');
			$data['username'] = $check_data['username'];
			$data['func'] = str_replace(' ', '', $check_data['func']);

			$this->view['main'] =  $this->load->view('safe/checklst',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			
			$this->load->view('login_view');
		}
		
	}

	public function Complete(){

		if($this->session->userdata('logged_in')) { 	

			$check_data = $this->session->userdata('logged_in');
			$data['username'] = $check_data['username'];
			$data['func'] = str_replace(' ', '', $check_data['func']);
			
			$this->view['main'] =  $this->load->view('safe/comp',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		

	}

	public function InsertUsers($username = null, $firstname = null, $lastname = null, $position = null, $dept = null )
    {
        require_once('nusoap/nusoap.php');

        $check_data = $this->session->userdata('logged_in');

        $username = $check_data['username'];
        $firstname = $check_data['firstname'];
        $lastname = $check_data['lastname'];
        $position = $check_data['position'];
        $dept = $check_data['dept'];

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');
        $client->soap_defencoding = 'UTF-8';
		$client->decode_utf8 = false;

		/*$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}*/


		$answer = $client->call('InsertUsers',array( 'username' => $username, 'firstname' => $firstname, 'lastname' => $lastname, 'position' => $position, 'dept' => $dept), '', '', false, true);

		/*$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }


		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}*/

		//echo $jsonData;
		//return false;
    }

	
}
