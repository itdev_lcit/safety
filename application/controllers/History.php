<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('Welcome.php');

class History extends Welcome {

	public function index(){

		if($this->session->userdata('logged_in')) { 	

			$check_data = $this->session->userdata('logged_in');
			$data['username'] = $check_data['username'];
			$data['func'] = str_replace(' ', '', $check_data['func']);

			$this->view['main'] =  $this->load->view('history/index', $data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}

	public function CheckList($id_loc = null){

		if($this->session->userdata('logged_in')) { 	

			$data['id_loc'] = $id_loc;
			$check_data = $this->session->userdata('logged_in');
			$data['func'] = str_replace(' ', '', $check_data['func']);

			$this->view['main'] =  $this->load->view('safe/checklst',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}

	public function Resume($id_detail = null,$id_key_form = null){

		if($this->session->userdata('logged_in')) { 	


			$check_data = $this->session->userdata('logged_in');
			
			$data['func'] = str_replace(' ', '', $check_data['func']);

			$data['user_id'] = $check_data['user_id'];
			$data['id_detail'] = $id_detail;
			$data['id_key_form'] = $id_key_form;

			$this->view['main'] =  $this->load->view('history/resume_checklst',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}

	public function Complete(){

		if($this->session->userdata('logged_in')) { 	

			$this->view['main'] =  $this->load->view('safe/comp','',true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		
	}

	public function DetailList($id_key_form = null){

		if($this->session->userdata('logged_in')) { 	

			$check_data = $this->session->userdata('logged_in');
			
			$data['func'] = str_replace(' ', '', $check_data['func']);

			$data['id_key_form'] = $id_key_form;

			$this->view['main'] =  $this->load->view('history/detail',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		
	}

	
}
