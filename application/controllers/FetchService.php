<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('Welcome.php');

class FetchService extends Welcome {

	 function __construct()
	 {
	   parent::__construct();
	   $this->load->library('session');

		
	 }

	public function LocationService()
    {
        require_once('nusoap/nusoap.php');

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');


		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$check_data = $this->session->userdata('logged_in');

		$answer = $client->call('LocationService',array( 'func' => $check_data['func']), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }

		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }



    public function DeptService()
    {
        require_once('nusoap/nusoap.php');

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');


		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$check_data = $this->session->userdata('logged_in');

		$answer = $client->call('DeptService',array('func' => $check_data['func']), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }

		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }


    public function InsertLoc()
    {
        require_once('nusoap/nusoap.php');

        $detail_loc = $this->input->post('loc_name');
        $id_fact = '0';

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');


		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$check_data = $this->session->userdata('logged_in');

		$answer = $client->call('InsertLoc',array('detail_loc' => $detail_loc, 'id_fact' => $id_fact, 'func' => $check_data['func']), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }

		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }
	
	public function UpdateLoc()
    {
        require_once('nusoap/nusoap.php');

        $id_loc = $this->input->post('edit_id_loc');
        $detail_loc = $this->input->post('loc_name');

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');


		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$check_data = $this->session->userdata('logged_in');

		$answer = $client->call('UpdateLoc',array('id_loc' => $id_loc, 'detail_loc' => $detail_loc, 'func' => $check_data['func']), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }

		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }

	public function DelLoc()
    {
        require_once('nusoap/nusoap.php');

        $id_loc = $this->input->post('id_loc');

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');


		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$check_data = $this->session->userdata('logged_in');

		$answer = $client->call('DelLoc',array('id_loc' => $id_loc, 'func' => $check_data['func']), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }

		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }

    public function EditDept()
    {
        require_once('nusoap/nusoap.php');

        $id_fact = $this->input->post('id_fact');
        $id_fact = $this->input->post('id_fact');

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');


		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$check_data = $this->session->userdata('logged_in');

		$answer = $client->call('DelDept',array('id_fact' => $id_fact, 'func' => $check_data['func']), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }

		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }

    public function DelDept()
    {
        require_once('nusoap/nusoap.php');

        $id_fact = $this->input->post('id_fact');

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');


		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$check_data = $this->session->userdata('logged_in');

		$answer = $client->call('DelDept',array('id_fact' => $id_fact, 'func' => $check_data['func']), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }

		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }


    public function InsertFact()
    {
        require_once('nusoap/nusoap.php');


        $dept_name = $this->input->post('dept_name');

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');


		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}
		$check_data = $this->session->userdata('logged_in');


		$answer = $client->call('InsertFact',array('detail_fact' => $dept_name, 'func' => $check_data['func']), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }

		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }

    public function UpdateFact()
    {
        require_once('nusoap/nusoap.php');


        $detail_fact = $this->input->post('dept_name');
        $id_fact = $this->input->post('edit_id_fact');

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');


		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$check_data = $this->session->userdata('logged_in');

		$answer = $client->call('UpdateFact',array('id_fact' => $id_fact, 'detail_fact' => $detail_fact, 'func' => $check_data['func']), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }

		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }


    public function LocSubjectService()
    {
        require_once('nusoap/nusoap.php');

        $description = $this->input->post('search_des');

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');
        $client->soap_defencoding = 'UTF-8';
		$client->decode_utf8 = false;

		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$check_data = $this->session->userdata('logged_in');

		$answer = $client->call('LocSubjectService',array('description' => $description, 'func' => $check_data['func']), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }


		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }

    public function InsertDetail()
    {
        require_once('nusoap/nusoap.php');

        $id_loc = $this->input->post('id_loc');
        $id_dept = $this->input->post('id_dept');
        $description = $this->input->post('description');

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');
        $client->soap_defencoding = 'UTF-8';
		$client->decode_utf8 = false;

		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$check_data = $this->session->userdata('logged_in');

		$answer = $client->call('InsertDetail',array('id_loc' => $id_loc, 'description' => $description, 'id_dept' => $id_dept, 'func' => $check_data['func']), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }

		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }

     public function UpdateDetail()
    {
        require_once('nusoap/nusoap.php');

        $id_detail = $this->input->post('edit_id_detail');
        $description = $this->input->post('description');
        $id_loc = $this->input->post('id_loc');
        $id_dept = $this->input->post('id_dept');

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');
        $client->soap_defencoding = 'UTF-8';
		$client->decode_utf8 = false;

		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$check_data = $this->session->userdata('logged_in');

		$answer = $client->call('UpdateDetail',array('id_detail' => $id_detail, 'description' => $description, 'id_loc' => $id_loc, 'id_dept' => $id_dept, 'func' => $check_data['func']), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }

		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }

     public function DelDetail()
    {
        require_once('nusoap/nusoap.php');

        $id_detail = $this->input->post('id_detail');

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');


		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$check_data = $this->session->userdata('logged_in');

		$answer = $client->call('DelDetail',array('id_detail' => $id_detail, 'func' => $check_data['func']), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }

		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }

    public function FormService()
    {
        require_once('nusoap/nusoap.php');

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');
        $client->soap_defencoding = 'UTF-8';
		$client->decode_utf8 = false;

		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$check_data = $this->session->userdata('logged_in');

		$answer = $client->call('FormService',array( 'func' => $check_data['func'] ), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }

		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }


    public function SubjectService()
    {
        require_once('nusoap/nusoap.php');

        $id_loc = $this->input->post('id_loc');

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');
        $client->soap_defencoding = 'UTF-8';
		$client->decode_utf8 = false;

		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$check_data = $this->session->userdata('logged_in');

		$answer = $client->call('SubjectService',array('id_loc' => $id_loc, 'func' => $check_data['func']), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }


		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }

    public function NextCheckListtService()
    {
        require_once('nusoap/nusoap.php');

        $id_loc = $this->input->post('id_loc');

        $rows_off = $this->input->post('cur_no')+1;
        $last_no = $this->input->post('last_no');

        $ck_row = $rows_off-1;

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');
        $client->soap_defencoding = 'UTF-8';
		$client->decode_utf8 = false;

		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$check_data = $this->session->userdata('logged_in');

		$answer = $client->call('CheckListtService',array('id_loc' => $id_loc, 'rows_off' => $ck_row, 'func' => $check_data['func']), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }


		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }


    public function PreCheckListtService()
    {
        require_once('nusoap/nusoap.php');

        $id_loc = $this->input->post('id_loc');

        $rows_off = $this->input->post('cur_no')-1;
        $last_no = $this->input->post('last_no');

        $ck_row = $rows_off-1;

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');
        $client->soap_defencoding = 'UTF-8';
		$client->decode_utf8 = false;

		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$check_data = $this->session->userdata('logged_in');

		$answer = $client->call('CheckListtService',array('id_loc' => $id_loc, 'rows_off' => $ck_row, 'func' => $check_data['func']), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }


		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }


    public function InsertKeyCheck()
    {
        require_once('nusoap/nusoap.php');

        $id_loc = $this->input->post('id_loc');
        $id_user = $this->input->post('id_user');


        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');
        $client->soap_defencoding = 'UTF-8';
		$client->decode_utf8 = false;

		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$check_data = $this->session->userdata('logged_in');

		$answer = $client->call('InsertKeyCheck',array('id_loc' => $id_loc, 'id_user' => $id_user, 'func' => $check_data['func']), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }


		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }

    public function InsertProcessCheck()
    {
        require_once('nusoap/nusoap.php');

        $id_loc = $this->input->post('id_loc');
        $id_user = $this->input->post('id_user');
        $id_detail = $this->input->post('id_detail');
		$id_key_form = $this->input->post('id_key_form');
		$score = $this->input->post('score');
		$remark = $this->input->post('remark');
		$path_img = $this->input->post('path_img');


        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');
        $client->soap_defencoding = 'UTF-8';
		$client->decode_utf8 = false;

		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$check_data = $this->session->userdata('logged_in');

		$answer = $client->call('InsertProcessCheck',array('id_detail' => $id_detail, 'id_loc' => $id_loc, 'id_user' => $id_user, 'id_key_form' => $id_key_form, 'score' => $score, 'remark' => $remark, 'path_img' => $path_img, 'func' => $check_data['func']), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }


		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }

    function ajaxImageStore()  
    {  
         if(isset($_FILES["image_file"]["name"]))  
         {  
         	  $this->load->library('image_lib');
         	  $new_name = time();
			  $config['file_name'] = $new_name;
              $config['upload_path'] = './public/img/';  
              $config['allowed_types'] = 'jpg|jpeg|png|gif';
              $config['width']     = '75';
    		  $config['height']   = '50';  

    		  $this->image_lib->clear();
		      $this->image_lib->initialize($config);
		      $this->image_lib->resize();

              $this->load->library('upload', $config);  
              if(!$this->upload->do_upload('image_file'))  
              {  
                  $error =  $this->upload->display_errors(); 
                  echo json_encode(array('msg' => $error, 'success' => false));
              }  
              else 
              {  

              	$upload_data =   $this->upload->data();

	            $configer =  array(
	              'image_library'   => 'gd2',
	              'source_image'    =>  $upload_data['full_path'],
	              'maintain_ratio'  =>  TRUE,
	              'width'           =>  250,
	              'height'          =>  250,
	            );
	            $this->image_lib->clear();
	            $this->image_lib->initialize($configer);
	            $this->image_lib->resize();


              	//$upload_data = $this->upload->data(); 
				$file_name = $upload_data['file_name'];
                $arr = array('msg' => 'Upload Complete !!','filesname' => $file_name, 'success' => false);
                echo json_encode($arr);
              }  
         }  
    } 

    public function ListService()
    {
        require_once('nusoap/nusoap.php');

        $id_user = $this->input->post('id_user');

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');
        $client->soap_defencoding = 'UTF-8';
		$client->decode_utf8 = false;

		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$check_data = $this->session->userdata('logged_in');

		$answer = $client->call('ListService',array('id_user' => $id_user, 'func' => $check_data['func']), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }


		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }

    public function ResumeListtService()
    {
        require_once('nusoap/nusoap.php');

        $id_detail = $this->input->post('id_detail');

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');
        $client->soap_defencoding = 'UTF-8';
		$client->decode_utf8 = false;

		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$check_data = $this->session->userdata('logged_in');

		$answer = $client->call('ResumeListtService',array('id_detail' => $id_detail, 'func' => $check_data['func']), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }


		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }

    public function DetailList()
    {
        require_once('nusoap/nusoap.php');

        $id_key_form = $this->input->post('id_key_form');

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');
        $client->soap_defencoding = 'UTF-8';
		$client->decode_utf8 = false;

		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$check_data = $this->session->userdata('logged_in');

		$answer = $client->call('DetailList',array('id_key_form' => $id_key_form, 'func' => $check_data['func']), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }


		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }

     public function SubjectDetailList()
    {
        require_once('nusoap/nusoap.php');

        $id_key_form = $this->input->post('id_key_form');

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');
        $client->soap_defencoding = 'UTF-8';
		$client->decode_utf8 = false;

		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$check_data = $this->session->userdata('logged_in');

		$answer = $client->call('SubjectDetailList',array('id_key_form' => $id_key_form, 'func' => $check_data['func']), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }


		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }

    public function VerifyListService()
    {
        require_once('nusoap/nusoap.php');

        $status_check = $this->input->post('status_check');

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');
        $client->soap_defencoding = 'UTF-8';
		$client->decode_utf8 = false;

		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$check_data = $this->session->userdata('logged_in');

		$answer = $client->call('VerifyListService',array('status_check' => $status_check, 'func' => $check_data['func']), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }


		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }

    public function SafetyStatus()
    {
        require_once('nusoap/nusoap.php');

        $id_process = $this->input->post('id_process');
        $safe_status = $this->input->post('safe_status');

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');
        $client->soap_defencoding = 'UTF-8';
		$client->decode_utf8 = false;

		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$check_data = $this->session->userdata('logged_in');

		$answer = $client->call('SafetyStatus',array( 'safe_status' => $safe_status, 'id_process' => $id_process), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }


		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }

    public function CheckUser()
    {
        require_once('nusoap/nusoap.php');


        $username = $this->input->post('username');

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');
        $client->soap_defencoding = 'UTF-8';
		$client->decode_utf8 = false;

		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$answer = $client->call('CheckUser',array( 'username' => $username), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }


		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }


    public function ListResolve()
    {
        require_once('nusoap/nusoap.php');

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');
        $client->soap_defencoding = 'UTF-8';
		$client->decode_utf8 = false;

		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}


		$check_data = $this->session->userdata('logged_in');

		$answer = $client->call('ListResolve',array('func' => $check_data['func']), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }

		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }


    public function SafetyResolve()
    {
        require_once('nusoap/nusoap.php');

        $id_detail = $this->input->post('id_detail');

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');
        $client->soap_defencoding = 'UTF-8';
		$client->decode_utf8 = false;

		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$check_data = $this->session->userdata('logged_in');

		$answer = $client->call('SafetyResolve',array( 'id_detail' => $id_detail, 'func' => $check_data['func']), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }


		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }

    public function SafetyResolveOther()
    {
        require_once('nusoap/nusoap.php');

        $id_process = $this->input->post('id_process');

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');
        $client->soap_defencoding = 'UTF-8';
		$client->decode_utf8 = false;

		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$check_data = $this->session->userdata('logged_in');

		$answer = $client->call('SafetyResolveOther',array( 'id_process' => $id_process, 'func' => $check_data['func']), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }


		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }

    public function DelKey()
    {
        require_once('nusoap/nusoap.php');

        $id_key_form = $this->input->post('id_key_form');

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');


		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$check_data = $this->session->userdata('logged_in');

		$answer = $client->call('DelKey',array('id_key_form' => $id_key_form, 'func' => $check_data['func']), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }

		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }


    public function ResetPassword()
    {
        require_once('nusoap/nusoap.php');

        $username = $this->input->post('username');

        $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');


		$error = $client->getError();
		if ($error) {
		    die("client construction error: {$error}\n");
		}

		$check_data = $this->session->userdata('logged_in');

		$answer = $client->call('ResetPassword',array('username' => $username), '', '', false, true);

		$error = $client->getError();
		if ($error) {
		    print_r($client->response);
		    print_r($client->getDebug());
		    die();
		 }

		foreach ($answer as $rs) {
		    $jsonData = $rs;
		}

		echo $jsonData;
		return false;
    }
	
}