<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function view(){

		$check_data = $this->session->userdata('logged_in');
		$data['name_user'] = $check_data['firstname'].'&nbsp;&nbsp;'.$check_data['lastname'];
		$data['role'] = $check_data['role'];
		$data['func'] = str_replace(' ', '', $check_data['func']);


		$this->view['header'] =  $this->load->view('mains/header', $data,true);
		$this->view['menu'] =  $this->load->view('mains/menu', $data,true);

		return $this->load->view('mains/index',$this->view);
	}
}
