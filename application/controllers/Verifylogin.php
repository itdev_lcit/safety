<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class VerifyLogin extends CI_Controller {
 
 function __construct()
 {
   parent::__construct();
 }
 
 function index()
 {

  $re_url = $this->input->post('re_url');

  if($this->session->userdata('logged_in')) { 

    $check_data = $this->session->userdata('logged_in');

    if($check_data['is_change'] == '1'){
        if($re_url != null OR $re_url != ''){
          redirect($re_url, 'refresh');
        } else {
          redirect('Safe', 'refresh');
        }
    } else {
          $this->load->helper(array('form'));
          $this->load->view('changepwd_view');

          //redirect('Maintenance', 'refresh'); 
    }
    
      

  } else {
    $this->load->library('form_validation');
   
     $this->form_validation->set_rules('username', 'Username', 'trim|required');
     $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_database');
   
     if($this->form_validation->run() == FALSE)
     {
       $this->load->view('login_view');
     }
     else
     {  
        $check_data = $this->session->userdata('logged_in');

        if($check_data['is_change'] == '1'){
            if($re_url != null OR $re_url != ''){
              redirect($re_url, 'refresh');
            } else {
              redirect('Safe', 'refresh');
            }
        } else {
            $this->load->helper(array('form'));
            $this->load->view('changepwd_view');

            //redirect('Maintenance', 'refresh'); 
        }
     }
  }
 
 }

  function check_database($password){
    require_once('nusoap/nusoap.php');

    $s_module = $this->input->post('s_module');
    $username = $this->input->post('username');
    $user_pwd = md5($password);

    $client = new nusoap_client('http://www.lcit.com/ServiceSafety.asmx?wsdl', 'WSDL');


    $error = $client->getError();
    if ($error) {
        die("client construction error: {$error}\n");
    }
    if($s_module == 'VC'){
      $answer = $client->call('VerifyUserVC',array('username' => $username, 'user_pwd' => $user_pwd), '', '', false, true);
    } else if($s_module == 'SP'){
      $answer = $client->call('VerifyUserSP',array('username' => $username, 'user_pwd' => $user_pwd), '', '', false, true);
    } else {
      $answer = $client->call('VerifyUserSC',array('username' => $username, 'user_pwd' => $user_pwd), '', '', false, true);
    }
    

    $error = $client->getError();
    if ($error) {
        print_r($client->response);
        print_r($client->getDebug());
        die();
     }

    foreach ($answer as $rs) {
        $jsonData = $rs;
    }

    $obj = json_decode($jsonData, true);



    if($obj){

       $sess_array = array();

       foreach($obj as $row)
       {
         $sess_array = array(
           'user_id' => $row['user_id'],
           'username' => $row['username'],
           'firstname' => $row['firstname'],
           'lastname' => $row['lastname'],
           'position' => $row['position'],
           'dept' => $row['dept'],
           'role' => $row['role'],
           'is_change' => $row['is_change'],
           'func' => $s_module
         );

         $this->session->set_userdata('logged_in', $sess_array);
         //redirect('VerifyLogin', 'refresh');
       }

       return TRUE;

    } else {
        $this->form_validation->set_message('check_database', 'Invalid username or password');
        return false;
    }
  }



 /*function check_database($password)
 {
   $username = $this->input->post('username');
 
   
    if(!empty($username) or $username != null){
      $adServer = "ldap://LCIT.com";

      $ldap = ldap_connect($adServer);
      $username = $this->input->post('username');
      $password = $this->input->post('password');

      $ldaprdn = 'LCIT' . "\\" . $username;

        ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);

        $bind = @ldap_bind($ldap, $ldaprdn, $password);

          if ($bind) {
            $filter="(sAMAccountName=$username)";
            $result = ldap_search($ldap,"dc=LCIT,dc=COM",$filter);
            ldap_sort($ldap,$result,"sn");
            $info = ldap_get_entries($ldap, $result);
              for ($i=0; $i<$info["count"]; $i++) {
                  if($info['count'] > 1)
                      break;
                      $sess_array = array(
                         'firstname' => $info[$i]["givenname"][0],
                         'lastname' => $info[$i]["sn"][0],
                         'position' => $info[$i]["title"][0],
                         'dept' => $info[$i]["description"][0],
                         'username' => $info[$i]["samaccountname"][0]
                       );
                    $this->session->set_userdata('logged_in', $sess_array);


                    redirect('Safe/Checkin', 'refresh');
                  }
                @ldap_close($ldap);
          } else {
              $this->form_validation->set_message('check_database', 'Invalid username or password');
              return false;
          }
    }
    
 }*/


}
?>