<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('Welcome.php');

class Verify extends Welcome {

	public function index(){

		if($this->session->userdata('logged_in')) { 	

			$check_data = $this->session->userdata('logged_in');
			
			$data['func'] = str_replace(' ', '', $check_data['func']);

			$this->view['main'] =  $this->load->view('verify/index',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}

	
	public function DetailList($id_key_form = null){

		if($this->session->userdata('logged_in')) { 	

			$check_data = $this->session->userdata('logged_in');
			
			$data['func'] = str_replace(' ', '', $check_data['func']);
			
			$data['id_key_form'] = $id_key_form;

			$this->view['main'] =  $this->load->view('verify/detail',$data,true);
			$this->view();

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}
		

	}

	
}
