<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include("Welcome.php");

class Logout extends Welcome {

	function __construct() {
        parent::__construct();
	}

	public function index() {
		$check_data = $this->session->userdata('logged_in');

		$this->session->unset_userdata('logged_in');
	    session_destroy();
	    redirect('login', 'refresh');
 	}

}	