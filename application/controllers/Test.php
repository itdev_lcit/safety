<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('Welcome.php');

class Test extends Welcome {

	public function __construct() {
        parent:: __construct();
        $this->load->helper("url");
        $this->load->library("pagination");
    }

	public function Index(){

		//$data['rs']  = $this->db->get('TestCon')->result_array();

		$this->view['main'] =  $this->load->view('test/index',$data,true);
		$this->view();

	}

}
