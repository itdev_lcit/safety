            <div class="col-md-4">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Safety Round</h4>
                  <p class="card-category">Setting Form</p>
                </div>
                <div class="card-body">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Form Name</label>
                          <input type="text" class="form-control" name="loc_name" id="loc_name" value="">
                        </div>
                      </div>
                    </div>
                    <button type="button" class="btn btn-primary pull-right" id="saveLoc">Save</button>
                    <div class="clearfix"></div>
                </div>
              </div>
            </div>
            <div class="col-md-8">
              <div class="card">
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                        <th>
                          No.
                        </th>
                        <th>
                          Form Name
                        </th>
                        <th style="text-align: right;">
                          Created
                        </th>
                        <th style="text-align: right;">
                          Updated
                        </th>
                        <th style="text-align: center;">
                          
                        </th>
                      </thead>
                      <tbody id="location_tb">
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>


<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
   $(document).ready(function () {

        $.ajax({
            url:'<?php echo site_url(); ?>FetchService/FormService',
            method:'POST',
        }).done(function(data){

            $('#location_tb').html('');

                    var o = JSON.parse(data);
                    var i = 0;
                    var no = 1;
                    var location_tb = '';

                    for(i=0; i < o.length; i++){
                        
                        var id_form =  o[i]['id_form']; 
                        var name_form =  o[i]['name_form'];
                        var created = o[i]['created'];
                        var updated = o[i]['updated'];


                        location_tb += '<tr class="r-location" data-id_form="'+id_form+'">';

                        location_tb += '<td style="text-align: center;">';
                        location_tb += no;
                        location_tb += '</td>';

                        location_tb += '<td>';
                        location_tb += name_form;
                        location_tb += '</td>';

                        location_tb += '<td style="text-align: right;">';
                        location_tb += created;
                        location_tb += '</td>';

                        location_tb += '<td style="text-align: right;">';
                        location_tb += updated;
                        location_tb += '</td>';

                        location_tb += '<td style="text-align: center;">';
                        location_tb += '<button style="cursor: pointer;" type="button" class="btn-sm btn-info del-loc"><i class="material-icons"> settings_applications </i></button>';
                        location_tb += '<button style="cursor: pointer;" type="button" class="btn-sm btn-danger del-loc"><i class="material-icons"> restore_from_trash </i></button>';
                        location_tb += '</td>';

                        location_tb += '</tr>';

                        no++;

                    }

                    $('#location_tb').html('');
                    $('#location_tb').append(location_tb)

        }); 



        $('#saveLoc').click(function(){

            var loc_name = $('#loc_name').val();
            var dept_name = $('#dept_name').val();

            $.ajax({
                url:'<?php echo site_url(); ?>FetchService/InsertLoc',
                method:'POST',
                data:{ loc_name:loc_name,dept_name:dept_name }
            }).done(function(data){

                alert('Success!!');
                location.reload();

            }); 
         }); 

        $("table").off("click", ".del-loc");
        $("table").on("click", ".del-loc", function(e) {
          e.preventDefault();

              var $row = $(this).parents('tr.r-location');
              var id_loc = $row.data('id_loc');


              var r = confirm("Confirm to delete ?");

              if (r == true) {

                $.ajax({
                    url:'<?php echo site_url(); ?>FetchService/DelLoc',
                    method:'POST',
                    data:{ id_loc:id_loc }
                }).done(function(data){

                    alert('Success!!');
                    location.reload();

                });
              }
                    
        });


    });
</script>