        <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title "><?php if($func == 'VC'){ echo 'Safety Round'; } else if ($func == 'SP') { echo 'Safety Platoon'; } else { echo 'Security Restricted'; } ?></h4>
                  <p class="card-category">Location -> <b id="loca_name"></b> ( <b id="b_no"></b>/<b id="b_last"></b> )</p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                        <table class="table">
                          <thead class=" text-primary">
                            <th style="width: 60%;">
                              Detail/Subject
                            </th>
                            <th style="text-align: center;">
                              Result<br>
                              NG | OK
                            </th>
                            <th>
                              Remark
                            </th>
                          </thead>
                          <tbody id="tb_detail">

                          </tbody>
                        </table>
                        <input type="hidden" id="cur_no">
                        <input type="hidden" id="last_no">
                        <input type="hidden" id="id_loc" value="<?php echo $id_loc; ?>">
                        <input type="hidden" id="id_detail">
                        <input type="hidden" id="id_key_form">
                        <input type="hidden" id="path_img">
                        <form method="post" id="upload_form" enctype="multipart/form-data">  
                          <input type="file" name="image_file" id="image_file" multiple="true" accept="image/*" id="finput">
                          <button class="btn btn-info">Upload</button>
                       </form>
                        <button type="button" class="btn btn-primary pull-right" id="next_rows">NEXT</button>
                        <button type="button" class="btn btn-success pull-right" id="fin_rows">FINISH</button>
                        <button type="button" class="btn btn-warning pull-right" id="pre_rows">BACK</button>
                  </div>
                </div>
              </div>
            </div>


<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">

   $(document).ready(function () {


    $('#upload_form').on('submit', function(e){  
           e.preventDefault();  
           if($('#image_file').val() == '')  
           {  
                alert("Please Select the File");  
           }  
           else 
           {  
                $.ajax({  
                     url:"<?php echo base_url(); ?>FetchService/ajaxImageStore",   
                     method:"POST",  
                     data:new FormData(this),  
                     contentType: false,  
                     cache: false,  
                     processData:false,  
                     dataType: "json",
                     success:function(res)  
                     {  
                        $('#path_img').val(res.filesname);
                        alert(res.msg);
                     }  
                });  
           }  
      });

      var id_loc = $('#id_loc').val();

        $.ajax({
            url:'<?php echo site_url(); ?>FetchService/NextCheckListtService',
            method:'POST',
            data:{  id_loc:id_loc },
        }).done(function(data){

            $('#tb_detail').html('');

                    var o = JSON.parse(data);
                    var i = 0;
                    var no = 1;
                    var location_tb = '';

                    for(i=0; i < o.length; i++){
                        
                        var id_detail =  o[i]['id_detail']; 
                        var detail =  o[i]['detail'];  
                        var detail_loc =  o[i]['detail_loc'];  
                        var RowID =  o[i]['RowID']; 
                        var All_Detail =  o[i]['All_Detail']; 
                        var id_loc =  o[i]['id_loc']; 


                        location_tb += '<tr class="r-location" data-id_loc="'+id_detail+'">';

                        location_tb += '<td>';
                        location_tb += detail;
                        location_tb += '</td>';

                        location_tb += '<td style="text-align: center;">';
                        location_tb += '<input type="radio" class="check_safe" name="check_safe" value="0" require="true"> ';
                        location_tb += ' | <input type="radio" class="check_safe" name="check_safe" value="1" require="true">';
                        location_tb += '</td>';



                        location_tb += '<td style="text-align: center;">';
                        location_tb += '<div class="form-group">';
                        location_tb += '<textarea class="form-control" rows="5" id="remark"></textarea>';
                        location_tb += '</div>';
                        location_tb += '</td>';

                        location_tb += '</tr>';

                        no++;

                         $.ajax({
                              url:'<?php echo site_url(); ?>FetchService/InsertKeyCheck',
                              method:'POST',
                              data:{ id_loc:id_loc}
                          }).done(function(data){

                              var o = JSON.parse(data);
                              var i = 0;
                              for(i=0; i < o.length; i++){

                                  var id_key_form =  o[i]['id_key_form']; 

                                  $('#id_key_form').val(id_key_form);
                              }

                          }); 

                    }
                    
                    $('#cur_no').val(RowID);
                    $('#last_no').val(All_Detail); 
                    $('#id_loc').val(id_loc);
                    $('#id_detail').val(id_detail);

                    $('#b_no').html(RowID);
                    $('#b_last').html(All_Detail);
                    $('#loca_name').html(detail_loc);
                    $('#tb_detail').html('');
                    $('#tb_detail').append(location_tb)


                    if(RowID == '1'){
                      $('#pre_rows').hide();
                    } else {
                      $('#pre_rows').show();
                    }

                    if(RowID == All_Detail){
                      $('#fin_rows').show();
                      $('#next_rows').hide();
                    } else {
                      $('#fin_rows').hide();
                    }



        }); 


        $('#next_rows').click(function(){


            var cur_no = $('#cur_no').val();
            var last_no = $('#last_no').val();
            var id_loc = $('#id_loc').val();
            var remark = $('#remark').val();
            var path_img = $('#path_img').val();
            var id_key_form = $('#id_key_form').val();
            var id_detail = $('#id_detail').val();
            var score = $(".check_safe:checked").val();

            if(score == null){
              alert('Please Check NG or OK !');
            } else {

               $.ajax({
                url:'<?php echo site_url(); ?>FetchService/InsertProcessCheck',
                method:'POST',
                data:{  id_loc:id_loc, 
                        id_detail:id_detail, 
                        id_key_form:id_key_form, 
                        score:score, 
                        remark:remark, 
                        path_img:path_img}
              }).done(function(data){


             }); 

            $.ajax({
                url:'<?php echo site_url(); ?>FetchService/NextCheckListtService',
                method:'POST',
                data:{ id_loc:id_loc,cur_no:cur_no, last_no:last_no }
            }).done(function(data){

                  var o = JSON.parse(data);
                    var i = 0;
                    var no = 1;
                    var location_tb = '';

                 for(i=0; i < o.length; i++){
                        
                        var id_detail =  o[i]['id_detail']; 
                        var detail =  o[i]['detail'];  
                        var detail_loc =  o[i]['detail_loc'];  
                        var RowID =  o[i]['RowID']; 
                        var All_Detail =  o[i]['All_Detail']; 
                        var id_loc =  o[i]['id_loc']; 


                        location_tb += '<tr class="r-location" data-id_loc="'+id_detail+'">';

                        location_tb += '<td>';
                        location_tb += detail;
                        location_tb += '</td>';

                        location_tb += '<td style="text-align: center;">';
                        location_tb += '<input type="radio" class="check_safe" name="check_safe" value="0" require="true"> ';
                        location_tb += ' | <input type="radio" class="check_safe" name="check_safe" value="1" require="true">';
                        location_tb += '</td>';


                        location_tb += '<td style="text-align: center;">';
                        location_tb += '<div class="form-group">';
                        location_tb += '<textarea class="form-control" rows="5" id="remark"></textarea>';
                        location_tb += '</div>';
                        location_tb += '</td>';

                        location_tb += '</tr>';

                        no++;

                    }
                    $('#path_img').val('');
                    $('#image_file').val('');

                    $('#cur_no').val(RowID);
                    $('#last_no').val(All_Detail); 
                    $('#id_loc').val(id_loc);
                    $('#id_detail').val(id_detail);

                    $('#b_no').html(RowID);
                    $('#b_last').html(All_Detail);
                    $('#loca_name').html(detail_loc);
                    $('#tb_detail').html('');
                    $('#tb_detail').append(location_tb)


                    if(RowID == '1'){
                      $('#pre_rows').hide();
                    } else {
                      $('#pre_rows').show();
                    }

                    if(RowID == All_Detail){
                      $('#fin_rows').show();
                      $('#next_rows').hide();
                    } else {
                      $('#fin_rows').hide();
                    }

            }); 

            }

         }); 


        $('#pre_rows').click(function(){

            var cur_no = $('#cur_no').val();
            var last_no = $('#last_no').val();
            var id_loc = $('#id_loc').val();

            $.ajax({
                url:'<?php echo site_url(); ?>FetchService/PreCheckListtService',
                method:'POST',
                data:{ id_loc:id_loc,cur_no:cur_no, last_no:last_no }
            }).done(function(data){

                  var o = JSON.parse(data);
                    var i = 0;
                    var no = 1;
                    var location_tb = '';

                 for(i=0; i < o.length; i++){
                        
                        var id_detail =  o[i]['id_detail']; 
                        var detail =  o[i]['detail'];  
                        var detail_loc =  o[i]['detail_loc'];  
                        var RowID =  o[i]['RowID']; 
                        var All_Detail =  o[i]['All_Detail']; 
                        var id_loc =  o[i]['id_loc']; 


                        location_tb += '<tr class="r-location" data-id_loc="'+id_detail+'">';

                        location_tb += '<td>';
                        location_tb += detail;
                        location_tb += '</td>';

                        location_tb += '<td style="text-align: center;">';
                        location_tb += '<input type="radio" class="check_safe" name="check_safe" value="0" require="true"> ';
                        location_tb += ' | <input type="radio" class="check_safe" name="check_safe" value="1" require="true">';
                        location_tb += '</td>';



                        location_tb += '<td style="text-align: center;">';
                        location_tb += '<div class="form-group">';
                        location_tb += '<textarea class="form-control" rows="5" id="remark"></textarea>';
                        location_tb += '</div>';
                        location_tb += '</td>';

                        location_tb += '</tr>';

                        no++;

                    }

                    $('#cur_no').val(RowID);
                    $('#last_no').val(All_Detail); 
                    $('#id_loc').val(id_loc);
                    $('#id_detail').val(id_detail);
                    $('#image_file').val('');

                    $('#b_no').html(RowID);
                    $('#b_last').html(All_Detail);
                    $('#loca_name').html(detail_loc);
                    $('#tb_detail').html('');
                    $('#tb_detail').append(location_tb)


                    if(RowID == '1'){
                      $('#pre_rows').hide();
                    } else {
                      $('#pre_rows').show();
                    }

                    if(RowID == All_Detail){
                      $('#fin_rows').show();
                      $('#next_rows').hide();
                    } else {
                      $('#fin_rows').hide();
                    }

            }); 
         }); 


        $('#fin_rows').click(function(){


            var cur_no = $('#cur_no').val();
            var last_no = $('#last_no').val();
            var id_loc = $('#id_loc').val();
            var remark = $('#remark').val();
            var path_img = $('#path_img').val();
            var id_key_form = $('#id_key_form').val();
            var id_detail = $('#id_detail').val();
            var score = $(".check_safe:checked").val();

            if(score == null){
              alert('Please Check NG or OK !');
            } else {

               $.ajax({
                url:'<?php echo site_url(); ?>FetchService/InsertProcessCheck',
                method:'POST',
                data:{  id_loc:id_loc, 
                        id_detail:id_detail, 
                        id_key_form:id_key_form, 
                        score:score, 
                        remark:remark, 
                        path_img:path_img}
              }).done(function(data){

                window.location = '<?php echo site_url(); ?>Safe/Complete';

             }); 

            }
            

        }); 


    });
</script>