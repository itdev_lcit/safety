
             <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title "><h4 class="card-title"><?php if($func == 'VC'){ echo 'Safety Round'; } else if ($func == 'SP') { echo 'Safety Platoon'; } else { echo 'Security Restricted'; } ?></h4></h4>
                  <p class="card-category">History</p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                        <th style="text-align: center;">
                          No.
                        </th>
                        <th>
                          Location
                        </th>
                        <th style="text-align: center;">
                          Status
                        </th>
                        <th style="text-align: center;">
                          A / Q
                        </th> 
                        <th>
                          Created
                        </th> 
                        <th>
                          
                        </th>
                      </thead>
                      <tbody id="location_tb">
                          <tr>
                              <td colspan="6" style="text-align: center;">-No Record-</td>
                          </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>


<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
   $(document).ready(function () {

    var username = '<?php echo $username; ?>';

      $.ajax({
            url:'<?php echo site_url(); ?>FetchService/CheckUser',
            method:'POST',
            data:{ username:username },
            contentType: "application/x-www-form-urlencoded;charset=utf-8",
        }).done(function(data){
                var o = JSON.parse(data);
                var i = 0;

            for(i=0; i < o.length; i++){

                  var id_user = o[i]['user_id'];
                  
                  $.ajax({
                        url:'<?php echo site_url(); ?>FetchService/ListService',
                        method:'POST',
                        data:{ id_user:id_user },
                        contentType: "application/x-www-form-urlencoded;charset=utf-8",
                    }).done(function(data){

                        $('#location_tb').html('');

                                var o = JSON.parse(data);
                                var i = 0;
                                var no = 1;
                                var location_tb = '';

                                for(i=0; i < o.length; i++){
                                    
                                    var id_key_form =  o[i]['id_key_form'];
                                    var detail_loc =  o[i]['detail_loc'];
                                    var status_check =  o[i]['status_check']; 
                                    var detail_fact =  o[i]['detail_fact'];
                                    var created =  o[i]['created'];   
                                    var Ass =  o[i]['Ass']; 
                                    var R_ass =  o[i]['R_ass']; 

                                    if(status_check == 'Wait'){
                                      var aprrove = '<i class="material-icons" style="color:orange;" title="Wait"> announcement </i> <br> Wait'; 
                                    } else {
                                      var aprrove = '<i class="material-icons" style="color:green;" title="Complete"> assignment_turned_in </i> <br> Complete';
                                    }

                                    location_tb += '<tr class="r-location" >';

                                    location_tb += '<td style="text-align: center;">';
                                    location_tb += no;
                                    location_tb += '</td>';

                                    location_tb += '<td>';
                                    location_tb += detail_loc;
                                    location_tb += '</td>';

                                    location_tb += '<td style="text-align: center;">';
                                    location_tb += aprrove;
                                    location_tb += '</td>';

                                    location_tb += '<td style="text-align: center;">';
                                    location_tb += Ass+' / '+R_ass;
                                    location_tb += '</td>';

                                    location_tb += '<td>';
                                    location_tb += created;
                                    location_tb += '</td>';

                                    location_tb += '<td>';
                                    location_tb += '<a href="<?php echo site_url(); ?>History/DetailList/'+id_key_form+'" target="_blank"><button type="button" rel="tooltip" title="View" class="btn btn-white btn-link btn-sm view-his"><i class="material-icons">visibility</i></button></a>';
                                    location_tb += '</td>';


                                    location_tb += '</tr>';

                                    no++;

                                }

                                $('#location_tb').html('');
                                $('#location_tb').append(location_tb)

                    }); 
             }
        }); 




    $('#start_asse').click(function(){

        
      var id_loc =  $('#id_loc').val();

      if(id_loc == '0' || id_loc == null){
        alert('Please Select Location');
      } else {
        window.location = '<?php echo site_url(); ?>Safe/CheckList/'+id_loc;
      }
       
     
        
    }); 


    });
</script>