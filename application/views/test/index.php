<style type="text/css">
  .label-danger {
    background-color: #db3325;
}
</style>
<div class="span12">
          <div class="widget widget-table action-table" id="search-bar">
            <!-- /widget-header -->
            <div class="widget-content">
              <br>
              <form class="form-inline" action="<?php echo site_url(); ?>Breakdown\BreakdownDetail\<?php echo $eq_id; ?>" method="POST">
                  &nbsp;&nbsp;
                  <label for="type">RQ Status</label>
                      <select class="form-control" id="type" name="type" style="width: 100px;">
                         <option value="All" <?php if($type == 'All'){ echo "selected"; } ?>>All</option>
                         <option value="None" <?php if($type == 'None'){ echo "selected"; } ?>>None</option>
                         <option value="Process" <?php if($type == 'Process'){ echo "selected"; } ?>>Process</option>
                         <option value="Wait" <?php if($type == 'Wait'){ echo "selected"; } ?>>Wait</option>
                        <option value="Complete" <?php if($type == 'Complete'){ echo "selected"; } ?>>Complete</option>
                      </select>
                  &nbsp;&nbsp;
                  &nbsp;&nbsp;
                  <label for="type">Number Eqiupment</label>
                      <select class="form-control" id="eq_no" name="eq_no" style="width: 120px;">
                        <option value="All">All</option>
                        <?php foreach ($eq_detail as $rs) { ?>
                          <option value="<?php echo $rs['eq_no']; ?>" <?php if($eq_no == $rs['eq_no']){ echo "selected"; } ?>><?php echo $rs['eq_name'].$rs['eq_no']; ?></option>
                        <?php } ?>
                      </select>
                  &nbsp;&nbsp;
                  <label for="type">Show</label>
                      <select class="form-control" id="show" name="show" style="width: 120px;">
                        <option value="10" <?php if($show == '10'){ echo "selected"; } ?>>10</option>
                        <option value="20" <?php if($show == '20'){ echo "selected"; } ?>>20</option>
                        <option value="30" <?php if($show == '30'){ echo "selected"; } ?>>30</option>
                        <option value="40" <?php if($show == '40'){ echo "selected"; } ?>>40</option>
                      </select>
                  &nbsp;&nbsp;
                  <button class="btn btn-info"><i class="icon-retweet"></i>&nbsp;Sort</button>
                  &nbsp;&nbsp;
                </form>
            </div>
            <!-- /widget-content --> 
          </div>

        <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Request Breakdown "<?php echo $eq_type->eq_name; ?>"</h3>
              <div align="right">
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered" style="font-size: 12px;">
                <thead>
                  <tr>
                    <th style="font-size: 12px;">
                     <div class="dropdown pull-left"> <a class="dropdown-toggle " id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="#">RQ <i class=" icon-caret-down"></i> </a>
                          <ul class="dropdown-menu " role="menu" aria-labelledby="dLabel">
                            <li><a href="#add-rq-bd" data-toggle="modal" data-target="#add-rq-bd"><i class=" icon-plus icon-small"></i>&nbsp; &nbsp; Add</a></li>
                          </ul>
                        </div>
                    </th>
                    <th style="font-size: 12px; text-align: left;">Number</th>
                    <th style="font-size: 12px; text-align: center;">Priority</th>
                    <th style="font-size: 12px; text-align: left;">BD Code</th>
                    <th style="font-size: 12px; text-align: center;">RQ Status</th>
                    <th style="font-size: 12px; text-align: left;">Lost Time</th>
                    <th style="font-size: 12px; text-align: left;">RQ Date</th>
                    <th style="font-size: 12px; text-align: left;">Accept Date</th>
                    <th style="font-size: 12px; text-align: left;">Complete Date</th>
                   
                      <th style="width: 15%;"> </th>
                   
                  </tr>
                </thead>
                <tbody>

                <?php if($rq_order) { ?>
                      <?php $i=1; foreach ($rq_order as $rs) { ?>

                      <tr class="r-eq" data-bd_detail="<?php echo $rs['bd_detail']; ?>" data-rq_id="<?php echo $rs['rq_id']; ?>">
                        <td>
                          <?php echo $rs['rq_id']; ?>
                        </td>
                        <td>
                          <?php echo $rs['eq_code'].$rs['eq_no']; ?>
                        </td>
                        <td style="text-align: center;">
                          
                           <?php if($rs['eq_priority'] == 'Low' ){ ?>
                              <span class="label label-warning"><?php echo $rs['eq_priority']; ?></span>
                            <?php } else if($rs['eq_priority'] == 'High' ) { ?>
                              <span class="label label-danger"><?php echo $rs['eq_priority']; ?></span>
                            <?php } ?>

                        </td>
                        <td>
                          <a href="#show" class="show-detail"><?php echo $rs['rq_code']; ?></a>
                        </td>
                        <?php if($user->role == "TECHNICIAN" OR $user->role == "ADMIN"){ ?>
                        <td style="text-align: center;">
                          <?php if($rs['rq_accept'] == 'None' ){ ?>
                             <a href="#add" class="update-status"><span class="label label-default"><?php echo $rs['rq_accept']; ?></span></a>
                            <?php } else if($rs['rq_accept'] == 'Process' ) { ?>
                              <a href="#add" class="update-status"><span class="label label-info"><?php echo $rs['rq_accept']; ?></span></a>
                            <?php } else if($rs['rq_accept'] == 'Complete' ) { ?>
                              <span class="label label-success" style="cursor: not-allowed;"><?php echo $rs['rq_accept']; ?></span>
                            <?php } else if($rs['rq_accept'] == 'Wait' ) { ?>
                              <a href="#add" class="update-status" title="<?php echo $rs['remark_w']; ?>"><span class="label label-warning"><?php echo $rs['rq_accept']; ?></span></a>
                            <?php } ?>
                        </td>
                      <?php } else { ?>

                        <td style="text-align: center;">
                          <?php if($rs['rq_accept'] == 'None' ){ ?>
                            <span class="label label-default" style="cursor: not-allowed;"><?php echo $rs['rq_accept']; ?></span>
                            <?php } else if($rs['rq_accept'] == 'Process' ) { ?>
                            <span class="label label-info"><?php echo $rs['rq_accept']; ?></span>
                            <?php } else if($rs['rq_accept'] == 'Complete' ) { ?>
                              <span class="label label-success" style="cursor: not-allowed;"><?php echo $rs['rq_accept']; ?></span>
                            <?php } else if($rs['rq_accept'] == 'Wait' ) { ?>
                              <span class="label label-warning" title="<?php echo $rs['remark_w']; ?>" style="cursor: not-allowed;"><?php echo $rs['rq_accept']; ?></span>
                            <?php } ?>
                        </td>

                      <?php } ?>
                        <td>
                          <?php 

                            if($rs['accept_date'] != ''){

                              if($rs['complete_date'] != ''){

                                $date = new DateTime($rs['accept_date']);
                                $now = new DateTime($rs['complete_date']);
                                echo $date->diff($now)->format("%d D: %h H: %i M");

                              } else {

                                $date = new DateTime($rs['accept_date']);
                                $now = new DateTime();
                                echo $date->diff($now)->format("%d D: %h H: %i M");

                              }

                            } else {
                              echo '-';
                            }

                          ?>
                        </td>
                        <td>
                          <?php echo date("j-M-y H:i", strtotime($rs['created'])); ?>
                        </td>
                        <td>
                          <?php 
                            if($rs['accept_date'] != ''){
                              echo date("j-M-y H:i", strtotime($rs['accept_date'])); 
                            } else {
                              echo '-';
                            }
                          ?>
                        </td>
                        <td>
                          <?php 
                            if($rs['complete_date'] != ''){
                              echo date("j-M-y H:i", strtotime($rs['complete_date'])); 
                            } else {
                              echo '-';
                            }
                          ?>
                        </td>
                       
                        <td class="td-actions">
                          <?php if($rs['rq_accept'] != 'None' ){ ?>
                          <a class="btn-small btn btn-info eye-eq-type" title="Log"><i class="icon-eye-open"> </i></a>
                          <?php } ?>
                            <?php if($user->role == "TECHNICIAN" OR $user->role == "ADMIN"){ ?>
                          <?php if($rs['rq_accept'] != 'Complete') { ?>
                          <a class="btn-small btn btn-danger del-eq-type" title="Delete"><i class="icon-trash icon-small"> </i></a>
                        <?php } ?>
                          <?php } ?>
                        </td>
                      
                      </tr>

                      <?php $i++;  } ?>
                  <?php } else { ?>
                    <tr >
                      <td colspan="11" style="text-align: center;">-No Request-</td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
</div>


 <!-- Add EQ -->
  <div class="modal fade" id="change-rq-status" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Change RQ Status</h4>
        </div>
        <div class="modal-body">
              <label for="eq_code">RQ Status:</label>
              <select class="form-control" id="rq_status" style="width: 500px;">
                  <option value="Process">Process</option>
                  <option value="Wait">Wait</option>
                  <option value="Complete">Complete</option>
              </select>
              <label for="bd_detail" id="remark_wait">Remark :</label>
              <textarea class="form-control" rows="5" id="wait_status" name="wait_status" style="width: 500px;"></textarea>
              <input type="hidden" id="change_rq_id" value="">

              <p id="msg-error-status" style="color: red;"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success save-rq-status" >Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

  <!-- Add EQ -->
  <div class="modal fade" id="detail-code" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Breakdown Detail</h4>
        </div>
        <div class="modal-body">
              <p id="desc_bd"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>


  <!-- Add EQ -->
  <div class="modal fade" id="add-rq-bd" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Request</h4>
        </div>
        <div class="modal-body">

              <label for="eq_code">Equipment Number:</label>
              <input type="hidden" id="eq_id" value="<?php echo $eq_id; ?>">
              <select class="form-control" id="eq_no" style="width: 500px;">
                  <option value="0">--</option>
                <?php foreach ($eq_detail as $rs) { ?>
                  <option value="<?php echo $rs['eq_no']; ?>"><?php echo $rs['eq_name'].$rs['eq_no']; ?></option>
                <?php } ?>
              </select>
              <label for="eq_code">Priority:</label>
              <select class="form-control" id="eq_priority" style="width: 500px;">
                  <option value="0">--</option>
                  <option value="Low">Low</option>
                  <option value="High">High</option>
              </select>
              <label for="eq_code">Breakdown System:</label>
              <select class="form-control" id="bd_system" style="width: 500px;">
                  <option value="0">--</option>
                <?php foreach ($bd_system as $rs) { ?>
                  <option value="<?php echo $rs['bd_id']; ?>"><?php echo $rs['bd_code']." : ".$rs['bd_name']; ?></option>
                <?php } ?>
              </select>
              <label for="eq_code">Breakdown Detail:</label>
              <select class="form-control" id="bd_detail" style="width: 500px;" >
              </select>
              <p id="msg-error-eq" style="color: red;"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success save-rq" >Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
      
    </div>
  </div>

      <!-- Delete EQ -->
  <div class="modal fade" id="del-eq-type" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Request Breakdown</h4>
        </div>
        <div class="modal-body">
              <p>Confirm to delete ?</p>
              <input type="hidden" id="del_rq_id" value="">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success cf-del-eq" >Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>
      
    </div>
  </div>


   <!-- Add EQ -->
  <div class="modal fade" id="detail-log" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Log Status Request</h4>
        </div>
        <div class="modal-body">
             <table class="table table-striped table-bordered" style="font-size: 12px;">
                <thead>
                  <tr>
                    <th style="font-size: 12px; text-align: center;">Origin Status</th>
                    <th style="font-size: 12px; text-align: center;">Modify Status</th>
                    <th style="font-size: 12px; text-align: left;">Name</th>
                    <th style="font-size: 12px; text-align: left;">Date</th>
                  </tr>
                </thead>
                <tbody id="log-status">
                </tbody>
            </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
 
<script src="<?php echo base_url(); ?>public/js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript">
$(document).ready(function(){

  $("#bd_detail").attr("disabled", true);

  $("#wait_status").hide();
  $("#remark_wait").hide();

   $('#rq_status').on('change', function (e) {

      var rq_status = $('#rq_status').val();

      if(rq_status == 'Wait'){
         $("#wait_status").show();
         $("#remark_wait").show();
      } else {
         $("#wait_status").hide();
         $("#remark_wait").hide();
      }

  });

  $('#bd_system').on('change', function (e) {

      var bd_system = $('#bd_system').val();

      var bd_detail = '';

      if(bd_system != '0'){

        $("#bd_detail").attr("disabled", true);
        $("#bd_detail").html('');

        $.ajax({
            type:'POST',
            url:'<?php echo site_url(); ?>Breakdown/GetBreakdownDetail',
            data:{ bd_system:bd_system}
          }).done(function(data){

              var o = JSON.parse(data);
              var i = 0;

              for(i=0; i < o.length; i++){

                bd_detail += '<option value="'+o[i]['bdd_id']+'">'+o[i]['bdd_code']+' : '+o[i]['bd_detail']+'</option>';

              }

              $("#bd_detail").append(bd_detail);

              $("#bd_detail").attr("disabled", false);

        });

      } else {

        $("#bd_detail").attr("disabled", true);
        $("#bd_detail").html('');

      }

  });

  $('.save-rq').click(function(){
       
      var eq_id = $('#eq_id').val();
      var eq_no = $('#eq_no').val();
      var eq_priority = $('#eq_priority').val();
      var bd_detail = $('#bd_detail').val();

      if(eq_no == '' || eq_priority == '0'){
        $('#msg-error-eq').html('*Please Input Equipment Number');
      } else if(eq_priority == '' || eq_priority == '0'){
        $('#msg-error-eq').html('*Please Choose Priority Request');
      } else if(bd_detail == '' || bd_detail == '0'){
        $('#msg-error-eq').html('*Please Choose Priority Request');
      } else {

        $.ajax({
          type:'POST',
          url:'<?php echo site_url(); ?>Breakdown/SaveRequestBreakdown',
          data:{ eq_id:eq_id, eq_no:eq_no, eq_priority:eq_priority, bd_detail:bd_detail}
        }).done(function(data){
            var o = JSON.parse(data);
            alert(o.msg);

            if(o.code_m == 'complete'){
              location.reload();
            }
        });

      }

  });   


  $("table").off("click", ".eye-eq-type");
  $("table").on("click", ".eye-eq-type", function(e) {
      e.preventDefault();


      var $row = $(this).parents('tr.r-eq');
      var rq_id = $row.data('rq_id');
      var log_status = '';
      $("#log-status").html('');


       $.ajax({
          type:'POST',
          url:'<?php echo site_url(); ?>Breakdown/GetLogStatus',
          data:{ rq_id:rq_id}
        }).done(function(data){
            var o = JSON.parse(data);
            var i = 0;

              for(i=0; i < o.length; i++){

                log_status += '<tr>';
                log_status += '<td style="font-size: 12px; text-align: center;">'+o[i]['origin_status']+'</td>';
                log_status += '<td style="font-size: 12px; text-align: center;">'+o[i]['modify_status']+'</td>';
                log_status += '<td>'+o[i]['name']+'</td>';
                log_status += '<td>'+o[i]['created']+'</td>';
                log_status += '</tr>';

              }

            $("#log-status").append(log_status);


        });

      $('#detail-log').modal('show');


  });

  $("table").off("click", ".show-detail");
  $("table").on("click", ".show-detail", function(e) {
      e.preventDefault();

      $('#desc_bd').html('');

      var $row = $(this).parents('tr.r-eq');
      var bd_detail = $row.data('bd_detail');

      $('#desc_bd').append(bd_detail);

       $('#detail-code').modal('show');


  });

  $("table").off("click", ".update-status");
  $("table").on("click", ".update-status", function(e) {
      e.preventDefault();

      var $row = $(this).parents('tr.r-eq');
      var rq_id = $row.data('rq_id');

      $('#change_rq_id').val(rq_id);
      
      $('#change-rq-status').modal('show');

  });

  $('.save-rq-status').click(function(){
       
      var rq_status = $('#rq_status').val();
      var rq_id = $('#change_rq_id').val();
      var wait_status = $('#wait_status').val();

      if(rq_status == 'Wait'){
        if(wait_status == null || wait_status == ''){
          $('#msg-error-status').html('*Please Input Remark.');
        } else {

          $.ajax({
            type:'POST',
            url:'<?php echo site_url(); ?>Breakdown/ChangeRequestStatus',
            data:{ rq_id:rq_id, rq_status:rq_status, wait_status:wait_status }
          }).done(function(data){
              var o = JSON.parse(data);
              alert(o.msg);

              if(o.code_m == 'complete'){
                location.reload();
              }
          });

        }
      } else {

        if(eq_no == '' || eq_priority == '0'){
          $('#msg-error-eq').html('*Please Input Equipment Number');
        } else if(eq_priority == '' || eq_priority == '0'){
          $('#msg-error-eq').html('*Please Choose Priority Request');
        } else if(bd_detail == '' || bd_detail == '0'){
          $('#msg-error-eq').html('*Please Choose Priority Request');
        } else {

          $.ajax({
            type:'POST',
            url:'<?php echo site_url(); ?>Breakdown/ChangeRequestStatus',
            data:{ rq_id:rq_id, rq_status:rq_status }
          }).done(function(data){
              var o = JSON.parse(data);
              alert(o.msg);

              if(o.code_m == 'complete'){
                location.reload();
              }
          });

      }

      }

  });

   $("table").off("click", ".del-eq-type");
  $("table").on("click", ".del-eq-type", function(e) {
      e.preventDefault();

      var $row = $(this).parents('tr.r-eq');
      var rq_id = $row.data('rq_id');

      $('#del_rq_id').val(rq_id);
      $('#del-eq-type').modal('show');

  });

  $('.cf-del-eq').click(function(){
       
      var del_rq_id = $('#del_rq_id').val();

        $.ajax({
          type:'POST',
          url:'<?php echo site_url(); ?>Breakdown/DelRequestBreakdown',
          data:{ del_rq_id:del_rq_id}
        }).done(function(data){
            var o = JSON.parse(data);
            alert(o.msg);

            if(o.code_m == 'complete'){
              location.reload();
            }
        })
  }); 

});
</script>