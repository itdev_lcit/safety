<div class="sidebar-wrapper">
        <ul class="nav">

          <li class="nav-item <?php if($this->uri->segment(1)=="Safe" ){echo 'active'; } ?>">
            <a class="nav-link" href="<?php echo site_url(); ?>Safe">
              <i class="material-icons">content_paste</i>
              <?php if($func == 'VC'){ echo 'Safety Round'; } else if ($func == 'SP') { echo 'Safety Platoon'; } else { echo 'Security Restricted'; } ?>
            </a>
          </li>
          <li class="nav-item <?php if($this->uri->segment(1)=="History" ){echo 'active'; } ?>">
            <a class="nav-link" href="<?php echo site_url(); ?>History">
              <i class="material-icons">library_books</i>
              <p>History</p>
            </a>
          </li>
    <?php if($role == 'ADMIN'){ ?>
          <li class="nav-item <?php if($this->uri->segment(1)=="Dept" ){echo 'active'; } ?>">
            <a class="nav-link" href="<?php echo site_url(); ?>Dept">
              <i class="material-icons">work</i>
              <p>Setting Dept.</p>
            </a>
          </li>

          <li class="nav-item <?php if($this->uri->segment(1)=="Loc" ){echo 'active'; } ?>">
            <a class="nav-link" href="<?php echo site_url(); ?>Loc">
              <i class="material-icons">my_location</i>
              <p>Setting Loc.</p>
            </a>
          </li>

          <li class="nav-item <?php if($this->uri->segment(1)=="Detail" ){echo 'active'; } ?>">
            <a class="nav-link" href="<?php echo site_url(); ?>Detail">
              <i class="material-icons">description</i>
              <p>Setting Subject.</p>
            </a>
          </li>

          <li class="nav-item <?php if($this->uri->segment(1)=="Verify" ){echo 'active'; } ?>">
            <a class="nav-link" href="<?php echo site_url(); ?>Verify">
              <i class="material-icons">verified_user</i>
              <p>Verify List</p>
            </a>
          </li>

          <li class="nav-item <?php if($this->uri->segment(1)=="Ng" ){echo 'active'; } ?>">
            <a class="nav-link" href="<?php echo site_url(); ?>Ng">
              <i class="material-icons">build</i>
              <p>NG List</p>
            </a>
          </li>

    <?php } ?>
        </ul>
      </div>