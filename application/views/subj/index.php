            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title"><?php if($func == 'VC'){ echo 'Safety Round'; } else if ($func == 'SP') { echo 'Safety Platoon'; } else { echo 'Security Restricted'; } ?></h4>
                  <p class="card-category">Setting Subject</p>
                </div>
                <div class="card-body">
                    <div class="row">
                      <div class="col-md-8">
                        <div class="form-group" id="detail_class">
                          <label class="bmd-label-floating">Detail/Subject</label>
                          <input type="text" class="form-control" name="description" id="description" value="">
                          <input type="hidden" id="edit_id_detail">
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <label class="bmd-label-floating">Department</label>
                          <select class="form-control" id="dept_name" name="dept_name" style="background-color: #202940;">

                          </select>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <label class="bmd-label-floating">Location</label>
                          <select class="form-control" id="loc_name" name="loc_name" style="background-color: #202940;">

                          </select>
                        </div>
                      </div>
                    </div>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <button type="button" class="btn btn-warning pull-right" id="clear-loc">Clear</button>
                    <button type="button" class="btn btn-primary pull-right" id="saveLoc">Save</button>

                    <div class="clearfix"></div>
                </div>
              </div>
            </div>

             <div class="col-md-5">
              <div class="card">
                <div class="card-header card-header-info">
                  <h4 class="card-title">Search </h4>
                  <p class="card-category"></p>
                </div>
                <div class="card-body">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Location</label>
                          <select class="form-control" id="search_des" name="search_des" style="background-color: #202940;">

                          </select>
                        </div>

                      </div>
                    </div>
                    <button type="button" class="btn btn-info pull-right" id="Search_subj">Search</button>
                    <div class="clearfix"></div>
                </div>
              </div>
            </div>

            <div class="col-md-12">
              <div class="card">
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                        <th style="text-align: center;">
                          No.
                        </th>
                        <th>
                          Detail/Subject
                        </th>
                        <th style="text-align: center;">
                          Dept.
                        </th>
                        <th style="text-align: center;">
                          Location
                        </th>
                        <th style="text-align: right;">
                          Created
                        </th>
                        <th style="text-align: right;">
                          Updated
                        </th>
                        <th style="text-align: center;">
                          
                        </th>
                      </thead>
                      <tbody id="location_tb">
                          <tr>
                              <td colspan="6" style="text-align: center;">-No Record-</td>
                          </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>


<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
   $(document).ready(function () {

    $('#clear-loc').click(function(){

        $('#edit_id_detail').val('');
        $('#description').val('');
        
      }); 


      $('#Search_subj').click(function(){
        var search_des = $('#search_des').val();
        $('#edit_id_detail').val('');
        $('#description').val('');
          $.ajax({
            url:'<?php echo site_url(); ?>FetchService/LocSubjectService',
            method:'POST',
            data:{ search_des:search_des },
            contentType: "application/x-www-form-urlencoded;charset=utf-8",
        }).done(function(data){

            $('#location_tb').html('');

                    var o = JSON.parse(data);
                    var i = 0;
                    var no = 1;
                    var location_tb = '';

                    for(i=0; i < o.length; i++){
                        
                        var id_detail =  o[i]['id_detail'];
                        var description =  o[i]['detail']; 
                        var detail_loc =  o[i]['detail_loc'];            
                        var detail_fact =  o[i]['detail_fact'];
                        var created = o[i]['created'];
                        var updated = o[i]['updated'];
                        var id_loc =  o[i]['id_loc'];
                        var id_dept =  o[i]['id_dept'];


                        location_tb += '<tr class="r-location" data-id_detail="'+id_detail+'" data-id_dept="'+id_dept+'" data-id_loc="'+id_loc+'" data-description="'+description+'">';

                        location_tb += '<td style="text-align: center;">';
                        location_tb += no;
                        location_tb += '</td>';

                        location_tb += '<td>';
                        location_tb += description;
                        location_tb += '</td>';

                        location_tb += '<td style="text-align: center;">';
                        location_tb += detail_fact;
                        location_tb += '</td>';

                        location_tb += '<td style="text-align: center;">';
                        location_tb += detail_loc;
                        location_tb += '</td>';

                        location_tb += '<td style="text-align: right;">';
                        location_tb += created;
                        location_tb += '</td>';

                        location_tb += '<td style="text-align: right;">';
                        location_tb += updated;
                        location_tb += '</td>';

                        location_tb += '<td style="text-align: center;">';
                        location_tb += '<button type="button" rel="tooltip" title="Edit" class="btn btn-white btn-link btn-sm edit-loc"><i class="material-icons" style="color:orange;">edit </i> </button>';
                        location_tb += '<button type="button" rel="tooltip" title="Remove" class="btn btn-white btn-link btn-sm del-loc"><i class="material-icons" style="color:red;">restore_from_trash </i> </button>';
                        location_tb += '</td>';

                        location_tb += '</tr>';

                        no++;

                    }

                    $('#location_tb').html('');
                    $('#location_tb').append(location_tb)

        }); 
      }); 


        $.ajax({
            url:'<?php echo site_url(); ?>FetchService/LocationService',
            method:'POST',
        }).done(function(data){

            $('#loc_name').html('');
            $('#search_des').html('');

                    var o = JSON.parse(data);
                    var i = 0;
                    var select_dept = '';

                    for(i=0; i < o.length; i++){
                        
                        var id_loc =  o[i]['id_loc']; 
                        var id_fact =  o[i]['id_fact'];            
                        var detail_fact =  o[i]['detail_fact'];
                        var detail_loc =  o[i]['detail_loc'];

                        select_dept += '<option value="'+ id_loc +'">';

                        select_dept += detail_loc;


                        select_dept += '</option>';

                    }

                    $('#loc_name').html('');
                    $('#loc_name').append(select_dept);

                    $('#search_des').html('');
                    $('#search_des').append(select_dept)

        }); 

        $.ajax({
            url:'<?php echo site_url(); ?>FetchService/DeptService',
            method:'POST',
        }).done(function(data){

            $('#dept_name').html('');

                    var o = JSON.parse(data);
                    var i = 0;
                    var select_dept = '';

                    for(i=0; i < o.length; i++){
                        
                        var id_fact =  o[i]['id_fact'];          
                        var detail_fact =  o[i]['detail_fact'];

                        select_dept += '<option value="'+ id_fact +'">';

                        select_dept += detail_fact;


                        select_dept += '</option>';

                    }

                    $('#dept_name').html('');
                    $('#dept_name').append(select_dept);


        }); 

        $('#saveLoc').click(function(){

            var id_dept = $('#dept_name').val();
            var id_loc = $('#loc_name').val();
            var description = $('#description').val();
            var edit_id_detail = $('#edit_id_detail').val();

            if(edit_id_detail){

              $.ajax({
                  url:'<?php echo site_url(); ?>FetchService/UpdateDetail',
                  method:'POST',
                  data:{ edit_id_detail:edit_id_detail, id_loc:id_loc, id_dept:id_dept,description:description }
              }).done(function(data){

                  alert('Success!!');
                  location.reload();

              }); 

            } else {

              $.ajax({
                  url:'<?php echo site_url(); ?>FetchService/InsertDetail',
                  method:'POST',
                  data:{ id_loc:id_loc, id_dept:id_dept ,description:description }
              }).done(function(data){

                  alert('Success!!');
                  location.reload();

              }); 

            }

            
         }); 

        $("table").off("click", ".del-loc");
        $("table").on("click", ".del-loc", function(e) {
          e.preventDefault();

              var $row = $(this).parents('tr.r-location');
              var id_detail = $row.data('id_detail');


              var r = confirm("Confirm to delete ?");

              if (r == true) {

                $.ajax({
                    url:'<?php echo site_url(); ?>FetchService/DelDetail',
                    method:'POST',
                    data:{ id_detail:id_detail }
                }).done(function(data){

                    alert('Success!!');
                    location.reload();

                });
              }
                    
        });

        $("table").off("click", ".edit-loc");
        $("table").on("click", ".edit-loc", function(e) {
          e.preventDefault();

              var $row = $(this).parents('tr.r-location');
              var id_loc = $row.data('id_loc'); 
              var id_dept = $row.data('id_dept');
              var id_detail = $row.data('id_detail');
              var description = $row.data('description');

              $("#dept_name").val(id_dept);
              $("#loc_name").val(id_loc);
              $("#edit_id_detail").val(id_detail); 
              $("#description").val(description);  
              
              $('#detail_class').addClass('is-focused');      
        });


    });
</script>