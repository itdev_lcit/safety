            <div class="col-md-4">
              <div class="card">
                <div class="card-header card-header-info">
                  <h4 class="card-title"><?php if($func == 'VC'){ echo 'Safety Round'; } else if ($func == 'SP') { echo 'Safety Platoon'; } else { echo 'Security Restricted'; } ?></h4>
                  <p class="card-category">Select Location</p>
                </div>
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Location</label>
                          <select class="form-control" id="dept_name" name="dept_name" style="background-color: #202940;">

                          </select>
                        </div>
                      </div>
                      <input type="hidden" name="id_loc" id="id_loc">
                    <button type="button" class="btn btn-info pull-right" id="search_loc">Search</button>
                    <button type="button" class="btn btn-primary pull-left" id="start_asse">Start</button>
                    <div class="clearfix"></div>
                </div>
              </div>
            </div>
             <div class="col-md-8">
              <div class="card">
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                        <th style="text-align: center;">
                          No.
                        </th>
                        <th>
                          Detail/Subject
                        </th>
                      </thead>
                      <tbody id="location_tb">
                          <tr>
                              <td colspan="6" style="text-align: center;">-No Record-</td>
                          </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>


<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
   $(document).ready(function () {

    $('#start_asse').prop('disabled', true);

       $('#search_loc').click(function(){
        $('#start_asse').prop('disabled', false);
        var id_loc = $('#dept_name').val();
        $('#id_loc').val(id_loc);

          $.ajax({
            url:'<?php echo site_url(); ?>FetchService/SubjectService',
            method:'POST',
            data:{ id_loc:id_loc },
            contentType: "application/x-www-form-urlencoded;charset=utf-8",
        }).done(function(data){

            $('#location_tb').html('');

                    var o = JSON.parse(data);
                    var i = 0;
                    var no = 1;
                    var location_tb = '';

                    for(i=0; i < o.length; i++){
                        

                        var id_detail =  o[i]['id_detail'];
                        var description =  o[i]['detail']; 
                        var detail_loc =  o[i]['detail_loc'];            
                        var detail_fact =  o[i]['detail_fact'];
                        var created = o[i]['created'];
                        var updated = o[i]['updated'];


                        location_tb += '<tr class="r-location" data-id_detail="'+id_detail+'">';

                        location_tb += '<td style="text-align: center;">';
                        location_tb += no;
                        location_tb += '</td>';

                        location_tb += '<td>';
                        location_tb += description;
                        location_tb += '</td>';

                        location_tb += '</tr>';

                        no++;

                    }

                    $('#location_tb').html('');
                    $('#location_tb').append(location_tb)

        }); 
      }); 
         $.ajax({
            url:'<?php echo site_url(); ?>FetchService/LocationService',
            method:'POST',
        }).done(function(data){

            $('#dept_name').html('');

                    var o = JSON.parse(data);
                    var i = 0;
                    var select_dept = '';

                    for(i=0; i < o.length; i++){
                        
                        var id_loc =  o[i]['id_loc'];          
                        var detail_loc =  o[i]['detail_loc'];

                        select_dept += '<option value="'+ id_loc +'">';

                        select_dept += detail_loc;


                        select_dept += '</option>';

                    }

                    $('#dept_name').html('');
                    $('#dept_name').append(select_dept)

        }); 


    $('#start_asse').click(function(){

        
      var id_loc =  $('#id_loc').val();

      if(id_loc == '0' || id_loc == null){
        alert('Please Select Location');
      } else {
        window.location = '<?php echo site_url(); ?>Safe/CheckList/'+id_loc;
      }
       
     
        
    }); 


    });
</script>