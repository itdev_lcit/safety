<div class="container-fluid">
          <div class="card">
            <div class="card-header card-header-success">
              <h4 class="card-title">Complete <?php if($func == 'VC'){ echo 'Safety Round'; } else if ($func == 'SP') { echo 'Safety Platoon'; } else { echo 'Security Restricted'; } ?></h4>
              <p class="card-category">Please checkout the
                <a href="<?php echo site_url(); ?>History"> <b>History <?php if($func == 'VC'){ echo 'Safety Round'; } else if ($func == 'SP') { echo 'Safety Platoon'; } else { echo 'Security Restricted'; } ?>.</b></a>
              </p>
            </div>
          </div>
</div>