
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Login / V-Checked&Security Restricted 
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="<?php echo base_url(); ?>public/assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="<?php echo base_url(); ?>public/assets/demo/demo.css" rel="stylesheet" />
</head>

<body class="dark-edition">

      <div class="content">
        <div class="container-fluid">

          <div class="row">
            <div class="col-md-5">
              
            </div>

            <div class="col-md-2">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">LOGIN</h4>
                </div>
                <div class="card-body">
                  <?php echo form_open('verifylogin'); ?>
                    <div class="row">
                    	
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Systems</label>
                          <select class="form-control" id="s_module" name="s_module" style="background-color: #202940;">
                          	<option value="VC">V-Checked</option>
                          	<option value="SC" <?php if($this->uri->segment(4) == '1'){ echo "selected"; } ?>>Restricted </option>
                            <option value="SP">Safety Platoon</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Username</label>
                          <input type="text" class="form-control" name="username">
                          <input type="hidden" name="re_url" value="<?php echo $this->uri->segment(1)."/".$this->uri->segment(2)."/".$this->uri->segment(3); ?>">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Password</label>
                          <input type="password" class="form-control" name="password">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <font color="red"><?php echo validation_errors(); ?></font>
                      </div>
                    </div>
                    
                    <button type="submit" class="btn btn-success pull-right">Login</button>
                    <div class="clearfix"></div>
                  </form>
                  <a href="<?php echo site_url(); ?>ChangePass/ResetPassword"><button type="button" class="btn btn-warning pull-right">Forget Password</button></a>
                </div>
              </div>
            </div>

            <div class="col-md-5">
             
            </div>

          </div>
        </div>
      </div>

  <!--   Core JS Files   -->
  <script src="<?php echo base_url(); ?>public/assets/js/core/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>public/assets/js/core/popper.min.js"></script>
  <script src="<?php echo base_url(); ?>public/assets/js/core/bootstrap-material-design.min.js"></script>
  <script src="https://unpkg.com/default-passive-events"></script>
  <script src="<?php echo base_url(); ?>public/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Place this tag in your head or just before your close body tag. -->
  <script async defer src="https://buttons.github.io/buttons.js"></script>

  <script src="<?php echo base_url(); ?>public/assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="<?php echo base_url(); ?>public/assets/js/material-dashboard.js?v=2.1.0"></script>


</body>

</html>