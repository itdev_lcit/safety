            <div class="col-md-4">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title"><?php if($func == 'VC'){ echo 'Safety Round'; } else if ($func == 'SP') { echo 'Safety Platoon'; } else { echo 'Security Restricted'; } ?></h4>
                  <p class="card-category">Setting Location</p>
                </div>
                <div class="card-body">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group" id="loc_class">
                          <label class="bmd-label-floating">Location Name</label>
                          <input type="text" class="form-control" name="loc_name" id="loc_name" value="">
                          <input type="hidden" id="edit_id_loc">
                        </div>
                      </div>
                    </div>
                    <button type="button" class="btn btn-primary pull-right" id="saveLoc">Save</button>
                    <div class="clearfix"></div>
                </div>
              </div>
            </div>
            <div class="col-md-8">
              <div class="card">
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                        <th>
                          No.
                        </th>
                        <th>
                          Location
                        </th>
                        <th style="text-align: right;">
                          Created
                        </th>
                        <th style="text-align: right;">
                          Updated
                        </th>
                        <th style="text-align: center;">
                          
                        </th>
                      </thead>
                      <tbody id="location_tb">
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>


<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
   $(document).ready(function () {

        $.ajax({
            url:'<?php echo site_url(); ?>FetchService/LocationService',
            method:'POST',
        }).done(function(data){

            $('#location_tb').html('');

                    var o = JSON.parse(data);
                    var i = 0;
                    var no = 1;
                    var location_tb = '';

                    for(i=0; i < o.length; i++){
                        
                        var id_loc =  o[i]['id_loc']; 
                        var detail_loc =  o[i]['detail_loc'];            
                        var detail_fact =  o[i]['detail_fact'];
                        var created = o[i]['created'];
                        var updated = o[i]['updated'];
                        var id_fact = o[i]['id_fact'];


                        location_tb += '<tr class="r-location" data-id_loc="'+id_loc+'" data-detail_loc="'+detail_loc+'" data-id_fact="'+id_fact+'">';

                        location_tb += '<td style="text-align: center;">';
                        location_tb += no;
                        location_tb += '</td>';

                        location_tb += '<td>';
                        location_tb += detail_loc;
                        location_tb += '</td>';

                        location_tb += '<td style="text-align: right;">';
                        location_tb += created;
                        location_tb += '</td>';

                        location_tb += '<td style="text-align: right;">';
                        location_tb += updated;
                        location_tb += '</td>';

                        location_tb += '<td style="text-align: center;">';
                        location_tb += '<button type="button" rel="tooltip" title="Edit" class="btn btn-white btn-link btn-sm edit-loc"><i class="material-icons" style="color:orange;">edit </i> </button>';
                        location_tb += '<button type="button" rel="tooltip" title="Remove" class="btn btn-white btn-link btn-sm del-loc"><i class="material-icons" style="color:red;">restore_from_trash </i> </button>';
                        location_tb += '</td>';

                        location_tb += '</tr>';

                        no++;

                    }

                    $('#location_tb').html('');
                    $('#location_tb').append(location_tb)

        }); 

        $.ajax({
            url:'<?php echo site_url(); ?>FetchService/DeptService',
            method:'POST',
        }).done(function(data){

            $('#dept_name').html('');

                    var o = JSON.parse(data);
                    var i = 0;
                    var select_dept = '';

                    for(i=0; i < o.length; i++){
                        
                        var id_fact =  o[i]['id_fact'];            
                        var detail_fact =  o[i]['detail_fact'];

                        select_dept += '<option value="'+id_fact+'">';

                        select_dept += detail_fact;


                        select_dept += '</option>';

                    }

                    $('#dept_name').html('');
                    $('#dept_name').append(select_dept)

        }); 

        $('#saveLoc').click(function(){

            var loc_name = $('#loc_name').val();
            var dept_name = $('#dept_name').val();
            var edit_id_loc = $('#edit_id_loc').val();

   

            if(edit_id_loc){
              $.ajax({
                  url:'<?php echo site_url(); ?>FetchService/UpdateLoc',
                  method:'POST',
                  data:{ edit_id_loc:edit_id_loc,loc_name:loc_name }
              }).done(function(data){

                  alert('Success!!');
                  location.reload();

              }); 
            } else {
              $.ajax({
                  url:'<?php echo site_url(); ?>FetchService/InsertLoc',
                  method:'POST',
                  data:{ loc_name:loc_name,dept_name:dept_name }
              }).done(function(data){

                  alert('Success!!');
                  location.reload();

              }); 
            }

            
         }); 

        $("table").off("click", ".del-loc");
        $("table").on("click", ".del-loc", function(e) {
          e.preventDefault();

              var $row = $(this).parents('tr.r-location');
              var id_loc = $row.data('id_loc');


              var r = confirm("Confirm to delete ?");

              if (r == true) {

                $.ajax({
                    url:'<?php echo site_url(); ?>FetchService/DelLoc',
                    method:'POST',
                    data:{ id_loc:id_loc }
                }).done(function(data){

                    alert('Success!!');
                    location.reload();

                });
              }
                    
        });

        $("table").off("click", ".edit-loc");
        $("table").on("click", ".edit-loc", function(e) {
          e.preventDefault();

              var $row = $(this).parents('tr.r-location');
              var id_loc = $row.data('id_loc'); 
              var id_fact = $row.data('id_fact');
              var detail_loc = $row.data('detail_loc');

              $("#dept_name").val(id_fact);
              $("#edit_id_loc").val(id_loc); 
              $("#loc_name").val(detail_loc);  
              
              $('#loc_class').addClass('is-focused');      
        });


    });
</script>