
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Reset Password
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="<?php echo base_url(); ?>public/assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="<?php echo base_url(); ?>public/assets/demo/demo.css" rel="stylesheet" />
</head>

<body class="dark-edition">

      <div class="content">
        <div class="container-fluid">

          <div class="row">
            <div class="col-md-5">
              
            </div>

            <div class="col-md-2">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Reset Password</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Username</label>
                          <input type="text" class="form-control" name="username" id="username">
                        </div>
                      </div>
                    </div>
                    
                    <button type="button" class="btn btn-success pull-right re-user">Reset</button>
                    <div class="clearfix"></div>
                </div>
              </div>
            </div>

            <div class="col-md-5">
              
            </div>

          </div>
        </div>
      </div>

  <!--   Core JS Files   -->
  <script src="<?php echo base_url(); ?>public/assets/js/core/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>public/assets/js/core/popper.min.js"></script>
  <script src="<?php echo base_url(); ?>public/assets/js/core/bootstrap-material-design.min.js"></script>
  <script src="https://unpkg.com/default-passive-events"></script>
  <script src="<?php echo base_url(); ?>public/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Place this tag in your head or just before your close body tag. -->
  <script async defer src="https://buttons.github.io/buttons.js"></script>

  <script src="<?php echo base_url(); ?>public/assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="<?php echo base_url(); ?>public/assets/js/material-dashboard.js?v=2.1.0"></script>


</body>

</html>


<script type="text/javascript">
   $(document).ready(function () {


      $('.re-user').click(function(){

          var username = $('#username').val();

          $.ajax({
              url:'<?php echo site_url(); ?>FetchService/ResetPassword',
              method:'POST',
              data:{ username:username },
              contentType: "application/x-www-form-urlencoded;charset=utf-8",
          }).done(function(data){
              alert('Reset Password Completed');
              window.location = '<?php echo site_url(); ?>';
          });
          
      }); 


    });
</script>