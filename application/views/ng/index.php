<style type="text/css">

  .modal-body{
    text-align: center;
    height: auto;

  }
  
}
</style>
             <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title "><?php if($func == 'VC'){ echo 'Safety Round'; } else if ($func == 'SP') { echo 'Safety Platoon'; } else { echo 'Security Restricted'; } ?></h4>
                  <p class="card-category">NG LIST</p>
                </div>
                <div class="card-body">
                  <div class="col-md-2">
                        <div class="form-group" id="loc_class">
                          <label class="bmd-label-floating">Search Code</label>
                          <input type="text" class="form-control" name="myInput" id="myInput" onkeyup="myFunction()" value="">
                        </div>
                    </div>
                  <div class="table-responsive">
                    <table class="table" id="myTable">
                      <thead class=" text-primary">
                        <th style="text-align: left;">
                          Code
                        </th>
                        <th width="10%">
                          Loc.
                        </th>
                        <th width="35%">
                          Description
                        </th>
                        <th width="25%">
                          Remark
                        </th>
                        <th>
                          Image
                        </th>
                        <th style="text-align: left;">
                          Date
                        </th>
                        <th style="text-align: center;">
                          
                        </th>
                      </thead>
                      <tbody id="location_tb">
                          <tr>
                              <td colspan="6" style="text-align: center;">-No Record-</td>
                          </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>


<div class="modal fade" id="show_img" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">              
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <img src="" class="imagepreview" style="width: 100%;" >
      </div>
    </div>
  </div>
</div>


<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>
<script type="text/javascript">
   $(document).ready(function () {


     $.ajax({
            url:'<?php echo site_url(); ?>FetchService/ListResolve',
            method:'POST',
            data:{ },
            contentType: "application/x-www-form-urlencoded;charset=utf-8",
        }).done(function(data){

            $('#location_tb').html('');

                    var o = JSON.parse(data);
                    var i = 0;
                    var no = 1;
                    var location_tb = '';

                    for(i=0; i < o.length; i++){
                        
                        var detail_loc =  o[i]['detail_loc'];
                        var description =  o[i]['description'];
                        var created =  o[i]['created'];
                        var remark =  o[i]['remark']; 
                        var path_img =  o[i]['path_img'];
                        var status_check =  o[i]['status_check'];  
                        var id_detail =  o[i]['id_detail']; 
                        var CodeNo =  o[i]['CodeNo']; 
                        var id_process =  o[i]['id_process']; 


                        if(path_img){
                          var check_img = '<button type="button" rel="tooltip" title="view-img" class="btn btn-white btn-link btn-sm view-img"><i class="material-icons"> collections </i></button>';
                        } else {
                          var check_img = '-';
                        }


                        location_tb += '<tr class="r-location" data-img="<?php echo base_url(); ?>public/img/'+path_img+'" data-detail_loc="'+detail_loc+'" data-id_detail="'+id_detail+'" data-description="'+description+'" data-id_process="'+id_process+'">';

                        location_tb += '<td style="text-align: center;">';
                        location_tb += CodeNo;
                        location_tb += '</td>';

                        location_tb += '<td>';
                        location_tb += detail_loc;
                        location_tb += '</td>';

                        location_tb += '<td>';
                        location_tb += description;
                        location_tb += '</td>';

                        location_tb += '<td>';
                        location_tb += remark;
                        location_tb += '</td>';

                        location_tb += '<td>';
                        location_tb += check_img;
                        location_tb += '</td>';

                        location_tb += '<td style="text-align: center;">';
                        location_tb += created;
                        location_tb += '</td>';


                        location_tb += '<td style="text-align: center;">';
                        location_tb += '<button type="button" rel="tooltip" title="Resolve" class="btn btn-green btn-link btn-sm aprove-list"><i class="material-icons" style="color:green;">construction</i></button>';
                        location_tb += '</td>';


                        location_tb += '</tr>';

                        no++;

                    }

                    $('#location_tb').html('');
                    $('#location_tb').append(location_tb)

        }); 

        

    $("table").off("click", ".aprove-list");
    $("table").on("click", ".aprove-list", function(e) {

                e.preventDefault();

                var $row = $(this).parents('tr.r-location');
                var id_detail = $row.data('id_detail'); 
                var description = $row.data('description'); 
                var detail_loc = $row.data('detail_loc'); 
                var id_process = $row.data('id_process');

                var result = confirm('Do you want to resolve location '+ detail_loc +' and topic "' + description + '" ?');
          

          if (result) {


              /*if(description != 'Others'){

                  $.ajax({
                      url:'<?php echo site_url(); ?>FetchService/SafetyResolve',
                      method:'POST',
                      data:{ id_detail:id_detail},
                      contentType: "application/x-www-form-urlencoded;charset=utf-8",
                  }).done(function(data){
                      alert('Completed Resolve.');
                      location.reload();
                  });

              } else {*/

                  $.ajax({
                      url:'<?php echo site_url(); ?>FetchService/SafetyResolveOther',
                      method:'POST',
                      data:{ id_process:id_process},
                      contentType: "application/x-www-form-urlencoded;charset=utf-8",
                  }).done(function(data){
                      alert('Completed Resolve.');
                      location.reload();
                  });

             /* } */

          } 
    });


    $("table").off("click", ".view-img");
    $("table").on("click", ".view-img", function(e) {

                e.preventDefault();

                var $row = $(this).parents('tr.r-location');
                var img = $row.data('img');

                $('.imagepreview').attr('src',img);
                $('#show_img').modal('show'); 
    });



    });
</script>

