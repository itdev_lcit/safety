            <div class="col-md-4">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title"> <?php if($func == 'VC'){ echo ' V-Checked '; } else if ($func == 'SP') { echo 'Safety Platoon'; } else { echo 'Security Restricted'; } ?></h4>
                  <p class="card-category">Setting Department</p>
                </div>
                <div class="card-body">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group" id="dep_class">
                          <label class="bmd-label-floating">Department</label>
                          <input type="text" class="form-control" name="dept_name" id="dept_name" value="" required="true">
                          <input type="hidden" id="edit_id_fact" value="">
                        </div>
                      </div>
                    </div>
                    <button type="button" class="btn btn-primary pull-right" id="saveLoc">Save</button>
                    <div class="clearfix"></div>
                </div>
              </div>
            </div>
            <div class="col-md-8">
              <div class="card">
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                        <th>
                          No.
                        </th>
                        <th>
                          Dept.
                        </th>
                        <th style="text-align: right;">
                          Created
                        </th>
                        <th style="text-align: right;">
                          Updated
                        </th>
                        <th style="text-align: center;">
                          
                        </th>
                      </thead>
                      <tbody id="location_tb">
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>


<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
   $(document).ready(function () {

        $.ajax({
            url:'<?php echo site_url(); ?>FetchService/DeptService',
            method:'POST',
        }).done(function(data){

            $('#location_tb').html('');

                    var o = JSON.parse(data);
                    var i = 0;
                    var no = 1;
                    var location_tb = '';

                    for(i=0; i < o.length; i++){
                        
                        var id_fact =  o[i]['id_fact'];
                        var detail_fact =  o[i]['detail_fact'];
                        var created = o[i]['created'];
                        var updated = o[i]['updated'];


                        location_tb += '<tr class="r-location" data-id_fact="'+id_fact+'" data-detail_fact="'+detail_fact+'">';

                        location_tb += '<td style="text-align: center;">';
                        location_tb += no;
                        location_tb += '</td>';

                        location_tb += '<td>';
                        location_tb += detail_fact;
                        location_tb += '</td>';

                        location_tb += '<td style="text-align: right;">';
                        location_tb += created;
                        location_tb += '</td>';

                        location_tb += '<td style="text-align: right;">';
                        location_tb += updated;
                        location_tb += '</td>';

                        location_tb += '<td style="text-align: center;">';
                        location_tb += '<button type="button" rel="tooltip" title="Edit" class="btn btn-white btn-link btn-sm edit-loc"><i class="material-icons" style="color:orange;">edit </i> </button>';
                        location_tb += '<button type="button" rel="tooltip" title="Remove" class="btn btn-white btn-link btn-sm del-loc"><i class="material-icons" style="color:red;">restore_from_trash </i> </button>';
                        location_tb += '</td>';

                        location_tb += '</tr>';

                        no++;

                    }

                    $('#location_tb').html('');
                    $('#location_tb').append(location_tb)

        }); 


        $('#saveLoc').click(function(){

            var dept_name = $('#dept_name').val();
            var edit_id_fact = $('#edit_id_fact').val();


            if(edit_id_fact){

              $.ajax({
                  url:'<?php echo site_url(); ?>FetchService/UpdateFact',
                  method:'POST',
                  data:{ edit_id_fact:edit_id_fact, dept_name:dept_name }
              }).done(function(data){

                  alert('Success!!');
                  location.reload();

              });

            } else {

             $.ajax({
                  url:'<?php echo site_url(); ?>FetchService/InsertFact',
                  method:'POST',
                  data:{ dept_name:dept_name }
              }).done(function(data){

                  alert('Success!!');
                  location.reload();

              });

            }

             
         }); 

        $("table").off("click", ".del-loc");
        $("table").on("click", ".del-loc", function(e) {
          e.preventDefault();

              var $row = $(this).parents('tr.r-location');
              var id_fact = $row.data('id_fact');


              var r = confirm("Confirm to delete ?");

              if (r == true) {

                $.ajax({
                    url:'<?php echo site_url(); ?>FetchService/DelDept',
                    method:'POST',
                    data:{ id_fact:id_fact }
                }).done(function(data){

                    alert('Success!!');
                    location.reload();

                });
              }
                    
        });


        $("table").off("click", ".edit-loc");
        $("table").on("click", ".edit-loc", function(e) {
          e.preventDefault();

              var $row = $(this).parents('tr.r-location');
              var id_fact = $row.data('id_fact');
              var detail_fact = $row.data('detail_fact');

              $('#dept_name').val(detail_fact);
              $('#edit_id_fact').val(id_fact);

              $('#dep_class').addClass('is-focused');
              
                    
        });

    });
</script>