             <div class="col-md-2">
              <div class="card">
                <div class="card-body">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Status</label>
                          <select class="form-control" id="search_des" name="search_des" style="background-color: #202940;">
                              <option >-Select-</option>
                              <option value="Wait">Wait</option>
                              <option value="Complete">Complete</option>
                          </select>
                        </div>

                      </div>
                    </div>
                    <button type="button" class="btn btn-info pull-right" id="Search_subj">Search</button>
                    <div class="clearfix"></div>
                </div>
              </div>
            </div>

             <div class="col-md-10">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title "><?php if($func == 'VC'){ echo 'Safety Round'; } else if ($func == 'SP') { echo 'Safety Platoon'; } else { echo 'Security Restricted'; } ?></h4>
                  <p class="card-category">Verify</p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                        <th style="text-align: center;">
                          No.
                        </th>
                        <th>
                          Location
                        </th>
                        <th>
                          User Account
                        </th>
                        <th style="text-align: center;">
                          Status
                        </th>
                        <th>
                          Created
                        </th>
                        <th>
                          
                        </th>
                      </thead>
                      <tbody id="location_tb">
                          <tr>
                              <td colspan="6" style="text-align: center;">-No Record-</td>
                          </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>


<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script type="text/javascript">
   $(document).ready(function () {

    $('#Search_subj').click(function(){
        var status_check = $('#search_des').val();
           $.ajax({
            url:'<?php echo site_url(); ?>FetchService/VerifyListService',
            method:'POST',
            data:{ status_check:status_check },
            contentType: "application/x-www-form-urlencoded;charset=utf-8",
        }).done(function(data){

            $('#location_tb').html('');

                    var o = JSON.parse(data);
                    var i = 0;
                    var no = 1;
                    var location_tb = '';

                    for(i=0; i < o.length; i++){
                        
                        var detail_loc =  o[i]['detail_loc'];
                        var username =  o[i]['username'];
                        var firstname =  o[i]['firstname'];
                        var lastname =  o[i]['lastname'];
                        var status_check =  o[i]['status_check'];   
                        var created =  o[i]['created'];  
                        var id_key_form =  o[i]['id_key_form']; 



                        if(status_check == 'Wait'){
                          var aprrove = '<i class="material-icons" style="color:orange;" title="Wait"> announcement </i> <br> Wait'; 
                        } else {
                          var aprrove = '<i class="material-icons" style="color:green;" title="Complete"> assignment_turned_in </i> <br> Complete';
                        }
                       

                        location_tb += '<tr class="r-location" >';

                        location_tb += '<td style="text-align: center;">';
                        location_tb += no;
                        location_tb += '</td>';

                        location_tb += '<td>';
                        location_tb += detail_loc;
                        location_tb += '</td>';

                        location_tb += '<td>';
                        location_tb += firstname + '&nbsp; &nbsp;' + lastname;
                        location_tb += '</td>';

                        location_tb += '<td style="text-align: center;">';
                        location_tb += aprrove;
                        location_tb += '</td>';

                        location_tb += '<td>';
                        location_tb += created;
                        location_tb += '</td>';

                        location_tb += '<td style="text-align: center;">';
                        location_tb += '<a href="<?php echo site_url(); ?>Verify/DetailList/'+id_key_form+'" target="_blank"><button type="button" rel="tooltip" title="View" class="btn btn-white btn-link btn-sm view-his"><i class="material-icons">visibility</i></button></a>';
                        location_tb += '</td>';


                        location_tb += '</tr>';

                        no++;

                    }

                    $('#location_tb').html('');
                    $('#location_tb').append(location_tb)

        }); 
      }); 




    $('#start_asse').click(function(){

        
      var id_loc =  $('#id_loc').val();

      if(id_loc == '0' || id_loc == null){
        alert('Please Select Location');
      } else {
        window.location = '<?php echo site_url(); ?>Safe/CheckList/'+id_loc;
      }
       
     
        
    }); 


    });
</script>