<style type="text/css">

  .modal-body{
    text-align: center;
    height: auto;

  }
  
 
}
</style>
             <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title "><?php if($func == 'VC'){ echo 'Safety Round'; } else if ($func == 'SP') { echo 'Safety Platoon'; } else { echo 'Security Restricted'; } ?></h4>
                  <p class="card-category">Verify</p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                        <th style="text-align: center;">
                          No.
                        </th>
                        <th width="45%">
                          Description
                        </th>
                        <th style="text-align: center;">
                          Checked
                        </th>
                        <th>
                          Remark
                        </th>
                        <th>
                          Image
                        </th>
                        <th style="text-align: center;">
                          Status
                        </th>
                        <th style="text-align: center;">
                          
                        </th>
                      </thead>
                      <tbody id="location_tb">
                          <tr>
                              <td colspan="6" style="text-align: center;">-No Record-</td>
                          </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>


<div class="modal fade" id="show_img" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">              
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <img src="" class="imagepreview" style="width: 100%;" >
      </div>
    </div>
  </div>
</div>


<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>

<script type="text/javascript">
   $(document).ready(function () {

    var id_key_form = "<?php echo $id_key_form; ?>";

     $.ajax({
            url:'<?php echo site_url(); ?>FetchService/DetailList',
            method:'POST',
            data:{ id_key_form:id_key_form },
            contentType: "application/x-www-form-urlencoded;charset=utf-8",
        }).done(function(data){

            $('#location_tb').html('');

                    var o = JSON.parse(data);
                    var i = 0;
                    var no = 1;
                    var location_tb = '';

                    for(i=0; i < o.length; i++){
                        
                        var description =  o[i]['description'];
                        var score =  o[i]['score'];
                        var remark =  o[i]['remark']; 
                        var path_img =  o[i]['path_img'];
                        var status_check =  o[i]['status_check'];  
                        var id_process =  o[i]['id_process']; 

                        if(score == 1){
                          var check_score = '<i class="material-icons" style="color:green;"> check_circle </i> <br> OK';
                        } else {
                          var check_score = '<i class="material-icons" style="color:red;"> cancel </i> <br> NG';
                        }

                        if(path_img){
                          var check_img = '<button type="button" rel="tooltip" title="view-img" class="btn btn-white btn-link btn-sm view-img"><i class="material-icons"> collections </i></button>';
                        } else {
                          var check_img = '-';
                        }

                        if(status_check == 1){
                          var aprrove = '<i class="material-icons" style="color:orange;" title="Wait"> announcement </i> <br> Wait'; 
                        } else if(status_check == 2) {
                          var aprrove = '<i class="material-icons" style="color:green;" title="Complete"> assignment_turned_in </i> <br> Approve';
                        } else {
                          var aprrove = '<i class="material-icons" style="color:red;" title="Cancel"> report_problem </i> <br> Cancel';
                        }
                       

                        location_tb += '<tr class="r-location" data-img="<?php echo base_url(); ?>public/img/'+path_img+'" data-id_process="'+id_process+'" >';

                        location_tb += '<td style="text-align: center;">';
                        location_tb += no;
                        location_tb += '</td>';

                        location_tb += '<td>';
                        location_tb += description;
                        location_tb += '</td>';

                        location_tb += '<td style="text-align: center;">';
                        location_tb += check_score;
                        location_tb += '</td>';

                        location_tb += '<td>';
                        location_tb += remark;
                        location_tb += '</td>';

                        location_tb += '<td>';
                        location_tb += check_img;
                        location_tb += '</td>';

                        location_tb += '<td style="text-align: center;">';
                        location_tb += aprrove;
                        location_tb += '</td>';


                        location_tb += '<td style="text-align: center;">';
                        location_tb += '<button type="button" rel="tooltip" title="Approve" class="btn btn-green btn-link btn-sm aprove-list"><i class="material-icons" style="color:green;">check_circle</i></button>';
                        location_tb += '<button type="button" rel="tooltip" title="Cancel" class="btn btn-green btn-link btn-sm cancel-list"><i class="material-icons" style="color:red;">remove_circle</i></button>';
                        location_tb += '</td>';


                        location_tb += '</tr>';

                        no++;

                    }

                    $('#location_tb').html('');
                    $('#location_tb').append(location_tb)

        }); 

        

    $("table").off("click", ".aprove-list");
    $("table").on("click", ".aprove-list", function(e) {

                e.preventDefault();

                var $row = $(this).parents('tr.r-location');
                var id_process = $row.data('id_process');
                var safe_status = '2'

                $.ajax({
                    url:'<?php echo site_url(); ?>FetchService/SafetyStatus',
                    method:'POST',
                    data:{ id_process:id_process , safe_status:safe_status},
                    contentType: "application/x-www-form-urlencoded;charset=utf-8",
                }).done(function(data){
                    location.reload();
                });

    });

    $("table").off("click", ".cancel-list");
    $("table").on("click", ".cancel-list", function(e) {

                e.preventDefault();

                var $row = $(this).parents('tr.r-location');
                var id_process = $row.data('id_process');
                var safe_status = '3'
                
                $.ajax({
                    url:'<?php echo site_url(); ?>FetchService/SafetyStatus',
                    method:'POST',
                    data:{ id_process:id_process , safe_status:safe_status },
                    contentType: "application/x-www-form-urlencoded;charset=utf-8",
                }).done(function(data){
                    location.reload();
                });
                
    });

    $("table").off("click", ".view-img");
    $("table").on("click", ".view-img", function(e) {

                e.preventDefault();

                var $row = $(this).parents('tr.r-location');
                var img = $row.data('img');

                $('.imagepreview').attr('src',img);
                $('#show_img').modal('show'); 
    });


    $('#start_asse').click(function(){

        
      var id_loc =  $('#id_loc').val();

      if(id_loc == '0' || id_loc == null){
        alert('Please Select Location');
      } else {
        window.location = '<?php echo site_url(); ?>Safe/CheckList/'+id_loc;
      }
       
     
        
    }); 


    });
</script>

